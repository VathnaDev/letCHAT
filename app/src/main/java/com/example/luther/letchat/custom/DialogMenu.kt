package com.example.luther.letchat.custom

import android.app.ActivityOptions
import android.view.View
import android.app.Dialog
import android.content.Context
import android.util.Log
import android.widget.TextView
import android.view.WindowManager
import android.view.Gravity
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import com.example.luther.letchat.R

public interface OnDialogItemClickListener {
    fun onItemClicked(result: Int,view: View)
}

class DialogMenu(
    private val context: Context,
    private val callback: OnDialogItemClickListener,
    private var type: Int = 1
) : View.OnClickListener {

    val dialog: Dialog = Dialog(context, R.style.DialogTheme)

    companion object {
        const val CAMERA: Int = 1
        const val BLOCK: Int = 2
        const val SEARCH: Int = 3
    }

    init {


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_picker)
        dialog.getWindow().setGravity(Gravity.BOTTOM)
        dialog.getWindow().setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog.show()

        var ivOne = dialog.findViewById(R.id.ivOptOne) as ImageView
        var tvOne = dialog.findViewById(R.id.tvOptOne) as TextView
        var ivTwo = dialog.findViewById(R.id.ivOptTwo) as ImageView
        var tvTwo = dialog.findViewById(R.id.tvOptTwo) as TextView

        if (type == BLOCK) {
            ivOne.setImageDrawable(context.resources.getDrawable(R.drawable.ic_mute))
            tvOne.text = "Mute"
            ivTwo.setImageDrawable(context.resources.getDrawable(R.drawable.ic_block))
            tvTwo.text = "Block"
        } else if (type == SEARCH) {
            ivOne.setImageDrawable(context.resources.getDrawable(R.drawable.ic_qr))
            tvOne.text = "Scan QR Code"
            ivTwo.setImageDrawable(context.resources.getDrawable(R.drawable.ic_search))
            tvTwo.text = "Search By ID"
        }

        ivOne.setOnClickListener(this)
        tvOne.setOnClickListener(this)
        ivTwo.setOnClickListener(this)
        tvTwo.setOnClickListener(this)
        dialog.findViewById<Button>(R.id.btnCancel).setOnClickListener {
            dialog.dismiss()
        }

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivOptOne, R.id.tvOptOne -> {
                this.callback.onItemClicked(1,view)
                dialog.dismiss()
            }
            R.id.ivOptTwo, R.id.tvOptTwo -> {
                this.callback.onItemClicked(2,view)
                dialog.dismiss()
            }
        }
    }
}