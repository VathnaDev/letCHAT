package com.example.luther.letchat.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.RemoteInput
import android.widget.Toast
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.data.model.Message
import com.google.gson.Gson
import org.json.JSONObject
import timber.log.Timber

class DirectReplyReceiver : BroadcastReceiver() {
    private val mGson = Gson()
    private val mSocket = LetChatApp.mSocket

    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.d(intent.toString())
        val replyText = RemoteInput.getResultsFromIntent(intent)?.getCharSequence(AppNotification.KEY_TEXT_REPLY)
        if (replyText.isNullOrEmpty()) return

        val senderId = intent!!.getIntExtra(AppNotification.SENDER_ID, 0)
        val receiverId = intent.getIntExtra(AppNotification.RECEIVER_ID, 0)
        sendMessage(replyText.toString(), senderId, receiverId)
        Toast.makeText(context, "Sent", Toast.LENGTH_LONG).show()
        AppNotification.mNotifyManager.cancel(
            intent.getIntExtra(AppNotification.KEY_NOTIFICATION_ID, 0)
        )
    }

    private fun sendMessage(message: String, senderId: Int, receiverId: Int) {
        val newMessage = Message(0, message, "", receiverId, senderId = senderId)
        mSocket.emit("chat", JSONObject(mGson.toJson(newMessage)))
    }
}
