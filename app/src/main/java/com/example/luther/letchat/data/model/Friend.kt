package com.example.luther.letchat.data.model

import java.util.*

data class Friend(
    val id: Int,
    val image: Any,
    val isOnline: Int,
    val lastOnlineAt: Date,
    val lastSeenAt: Date?,
    val name: String,
    val status: String,
    val uid: String
)