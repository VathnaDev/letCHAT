package com.example.luther.letchat.ui.setting


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.luther.letchat.R
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.utils.Const
import kotlinx.android.synthetic.main.fragment_setting.*

class SettingFragment : Fragment() {

    private lateinit var mViewModel: SettingFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel = ViewModelProviders.of(activity!!)
            .get(SettingFragmentViewModel::class.java)

        val allowNotify = Pref.getSharedPreferences(activity!!)
            .getBoolean(Const.ALLOW_NOTIFICATION, true)
        swNotification.isChecked = allowNotify

        initListeners()
        observeData()
    }

    private fun initListeners() {
        swNotification.setOnClickListener {
            mViewModel.toggleNotification()
        }
    }

    private fun observeData() {
        mViewModel.allowNotification.observe(this, Observer { isAllow ->
            isAllow?.let {
                swNotification.isChecked = it
                Pref.getEditor(activity!!)
                    .putBoolean(Const.ALLOW_NOTIFICATION, swNotification.isChecked)
                    .commit()
            }
        })
    }


}
