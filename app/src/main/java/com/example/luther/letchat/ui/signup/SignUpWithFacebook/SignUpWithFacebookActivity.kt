package com.example.luther.letchat.ui.signup.SignUpWithFacebook

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.ui.login.LoginActivity
import com.example.luther.letchat.ui.main.MainActivity
import com.example.luther.letchat.utils.ActionAlertDialogUtils
import com.example.luther.letchat.utils.ErrorDialogUtils
import com.facebook.AccessToken
import com.facebook.FacebookSdk
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.activity_sign_up_with_facebook.*

class SignUpWithFacebookActivity : AppCompatActivity() {
    private lateinit var viewModel: SignUpWithFacebookActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(this)
        setContentView(R.layout.activity_sign_up_with_facebook)

        setSupportActionBar(toolbar3)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = null

        viewModel = ViewModelProviders.of(this)
                .get(SignUpWithFacebookActivityViewModel::class.java)
        signUpProgressBar?.visibility = View.INVISIBLE

        uName = intent.getStringExtra("name")
        uImage = intent.getStringExtra("imageUrl")
        Glide.with(this).load(uImage).into(imgUserProfile)
        etName.setText(uName)
        setupListener()
        observeData()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (item.itemId == android.R.id.home) {
                checkFacebookLoginState()
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            val alertMessage = "Do you want to exit this activity?"
            ActionAlertDialogUtils.performActionAlertDialog(this, alertMessage, object : ActionAlertDialogUtils.ActionResultListener {
                override fun onResultYes() {
                    checkFacebookLoginState()
                    onBackPressed()
                }
            })
            false
        } else
            super.onKeyDown(keyCode, event)
    }

    private fun setupListener() {
        btnSignUp.setOnClickListener {
            val name: String = etName.text.toString()
            val id: String = etId.text.toString()
            val email: String = etEmail.text.toString()
            val tokenKey: String = intent.getStringExtra("tokenKey")
            if (name.isEmpty() || id.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                if (name.isEmpty()) {
                    etName.error = "Username cannot be blank!"
                }
                if (id.isEmpty()) {
                    etId.error = "User id cannot be blank!"
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    etEmail.error = "Email cannot be blank and must follow the standard form of email address!"
                }
                return@setOnClickListener
            }
            performSignUpWithFacebook(name, email, id, tokenKey)
        }
    }

    private fun performSignUpWithFacebook(name: String, email: String, id: String, tokenKey: String) {
        viewModel.registerUserWithFacebook(name, email, id, tokenKey)
    }

    fun observeData() {
        viewModel.user.observe(this, Observer { user ->
            user?.let {
                signUpProgressBar?.visibility = View.INVISIBLE
                LetChatApp.user = it
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            signUpProgressBar?.visibility = View.INVISIBLE
            val errorResponse = when {
                it.toString().contains("400") -> "Email or user id  already exist."
                it.toString().contains("502") -> "Weak connection!"
                else -> "Something went wrong. \nPlease try again later!"
            }
            ErrorDialogUtils.performErrorMessageDialog(this, errorResponse)
        })
    }

    private fun checkFacebookLoginState() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return //already sign out
        }
        GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permission/", null, HttpMethod.DELETE, GraphRequest.Callback {
            LoginManager.getInstance().logOut()
        }).executeAsync()
    }

    companion object {
        private lateinit var uName: String
        private lateinit var uImage: String
    }

}


