package com.example.luther.letchat.ui.chat

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.net.Uri
import android.widget.Toast
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.data.model.Message
import com.example.luther.letchat.data.model.User
import com.example.luther.letchat.data.repo.UserRepository
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import timber.log.Timber
import android.provider.MediaStore
import android.content.Context
import android.database.Cursor
import java.io.File


class ChatActivityViewModel(private val app: Application) : AndroidViewModel(app) {
    private val mGson = Gson()
    private val disposable = CompositeDisposable()
    private val mUserRepository: UserRepository = UserRepository(LetChatApiService.create(app))

    private val mMessageList = arrayListOf<Message>()
    private var mSocket: Socket = LetChatApp.mSocket
    private var mSender: User = LetChatApp.user


    private val _receiver = MutableLiveData<Friend>()
    val receiver: LiveData<Friend>
        get() = _receiver

    private val _messages = MutableLiveData<List<Message>>()
    val messages: LiveData<List<Message>>
        get() = _messages

    private val _status = MutableLiveData<String>()
    val status: LiveData<String>
        get() = _status

    private val _message = MutableLiveData<String>()
    val message: LiveData<String>
        get() = _message

    private val onErrorListener = Emitter.Listener { errors ->
        errors.forEach {
            Timber.d(it.toString())
        }
    }

    init {
        mSocket.on("chat") { messages ->
            val newMessages = mGson.fromJson(messages[0].toString(), Message::class.java)
            if (newMessages.senderId == receiver.value?.id) {
                mMessageList.add(newMessages)
                _messages.postValue(mMessageList)
                emitSeenChats(newMessages.receiverId)
            }
        }.on("exception", onErrorListener)

        mSocket.on("typing") {
            _status.postValue("Typing...")
        }.on("exception", onErrorListener)

        mSocket.on("stop-typing") { typing ->
            _receiver.postValue(_receiver.value)
            typing.forEach {
                Timber.d("STOP TYPING $it")
            }
        }.on("exception", onErrorListener)
    }

    fun sendMessage(message: String) {
        _receiver.value?.let {
            val newMessage = Message(0, message, "", it.id, senderId = mSender.id)
            mSocket.emit("chat", JSONObject(mGson.toJson(newMessage)))
                .on("exception", onErrorListener)
            mMessageList.add(mMessageList.size, newMessage)
            _messages.postValue(mMessageList)
        }
    }

    fun sendPhoto(file: File) {
        val request = RequestBody.create(
            MediaType.parse("image/${file.extension.replace("jpg", "jpeg")}"), file
        )
        val body = MultipartBody.Part.createFormData("image", "file", request)

        disposable.add(
            mUserRepository.uploadImage(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    _receiver.value?.let {
                        val newMessage =
                            Message(0, "", response.image.filename, it.id, senderId = mSender.id)
                        mSocket.emit("chat", JSONObject(mGson.toJson(newMessage)))
                            .on("exception", onErrorListener)
                        newMessage.gallery = file.absolutePath
                        mMessageList.add(mMessageList.size, newMessage)
                        _messages.postValue(mMessageList)
                    }
                }, {
                    Timber.e(it)
                })
        )
    }

    fun emitTypingEvent() {
        _receiver.value?.let {
            val data = JSONObject(
                mapOf(
                    "receiverUid" to receiver.value!!.uid
                )
            )
            mSocket.emit("typing", data)
                .on("exception", onErrorListener)
        }
    }

    fun emitStopTypingEvent() {
        _receiver.value?.let {
            val data = JSONObject(
                mapOf(
                    "receiverUid" to receiver.value!!.uid
                )
            )
            mSocket.emit("stop-typing", data)
                .on("exception", onErrorListener)
        }
    }

    private fun emitSeenChats(receiverId: Int) {
        val data = JSONObject(
            mapOf(
                "senderId" to receiverId
            )
        )
        Timber.d("SEEN: " + data.toString())
        mSocket.emit("seen", data)
            .on("exception", onErrorListener)
    }

    fun fetchMessages(userId: Int) {
        disposable.add(
            mUserRepository.getChatList(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mMessageList.addAll(it.chats)
                    _messages.postValue(mMessageList)
                }, { error ->
                    Toast.makeText(app, error.message, Toast.LENGTH_SHORT).show()
                })
        )
    }


    fun fetchReceiver(userId: Int) {
        disposable.add(
            mUserRepository.getFriend(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ receiver ->
                    Timber.d(receiver.toString())
                    _receiver.postValue(receiver).also {
                        emitSeenChats(receiver.id)
                        //For push notification check
                        LetChatApp.receiverId = receiver.id
                    }
                }, { error ->
                    Toast.makeText(getApplication(), error.message, Toast.LENGTH_SHORT).show()
                })
        )
    }


    fun muteChat() {
        receiver.value?.let { friend ->
            disposable.add(
                mUserRepository.muteChat(friend.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it) {
                            _message.postValue("Mute Chat Successfully")
                        } else {
                            _message.postValue("Unmute Chat Successfully")
                        }
                    }, {
                        Timber.d(it)
                    })
            )
        }
    }

    fun blockUser() {
        receiver.value?.let { friend ->
            disposable.add(
                mUserRepository.blockUser(friend.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        _message.postValue("Block User Successfully")
                    }, {
                        _message.postValue("User Already Block")
                    })
            )
        }
    }

    override fun onCleared() {
        if (!disposable.isDisposed)
            disposable.isDisposed
        super.onCleared()
    }

}