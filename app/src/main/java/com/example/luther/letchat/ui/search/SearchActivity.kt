package com.example.luther.letchat.ui.search

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.ui.search.adapter.SearchUserListAdapter
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity(), SearchUserListAdapter.EventCallback {
    private val userList = ArrayList<Friend>()
    private val mUserListAdapter = SearchUserListAdapter(userList, this)

    private lateinit var mViewModel: SearchActivityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        mViewModel = ViewModelProviders.of(this)
            .get(SearchActivityViewModel::class.java)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        rvSearchList.adapter = mUserListAdapter
        setupListener()
        observeData()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun setupListener() {

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mViewModel.searchUser(s.toString())
            }

        })
    }

    private fun observeData() {
        mViewModel.users.observe(this, Observer { listUsers ->
            listUsers?.let {
                userList.clear()
                userList.addAll(it)
                mUserListAdapter.notifyDataSetChanged()
            }
        })
    }

    override fun onAddFriendClickListener(user: Friend) {
        mViewModel.sendFriendRequest(user.uid)
    }
}

