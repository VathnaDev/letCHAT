package com.example.luther.letchat.ui.friend.list

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.data.repo.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class FriendListViewModel(private val app: Application) : AndroidViewModel(app) {
    private val mDisposable = CompositeDisposable()
    private val mUserRepository: UserRepository = UserRepository(LetChatApiService.create(app))


    private val _friends = MutableLiveData<List<Friend>>()
    val friends: LiveData<List<Friend>>
        get() = _friends

    init {
        fetchFriends()
    }

    private fun fetchFriends() {
        mDisposable.add(
            mUserRepository.searchFriend("", 99, 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d(it.toString())
                    _friends.postValue(it.data)
                }, {
                    Timber.e(it)
                })
        )
    }
}