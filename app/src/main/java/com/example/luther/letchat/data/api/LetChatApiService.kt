package com.example.luther.letchat.data.api

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.data.model.User
import com.example.luther.letchat.data.model.response.*
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.utils.Const
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Multipart
import timber.log.Timber


interface LetChatApiService {

    @FormUrlEncoded
    @POST("auth/sign-up")
    fun registerUser(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("uid") uid: String,
        @Field("password") password: String
    ): Observable<UserResponse>

    @FormUrlEncoded
    @POST("auth/sign-up")
    fun registerUserWithFacebook(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("uid") uid: String,
        @Field("accessToken") tokenKey: String
    ): Observable<UserResponse>

    @FormUrlEncoded
    @POST("auth/login")
    fun loginUser(
        @Field("email") email: String,
        @Field("password") password: String
    ): Observable<UserResponse>

    @GET("user")
    fun getCurrentUser(): Observable<User>

    @FormUrlEncoded
    @POST("auth/login/facebook")
    fun loginWithFacebook(
        @Field("accessToken") accessToken: String
    ): Observable<UserResponse>

    @GET("user/valid-uid")
    fun isValidUid(
        @Field("uid") uid: String
    ): Observable<Boolean>

    @GET("user/valid-email")
    fun isValidEmail(
        @Field("email") email: String
    ): Observable<Boolean>

    @PUT("/user/set-notification")
    fun toggleNotification(): Observable<Boolean>

    @GET("friend/search")
    fun searchUser(
        @Query("uid") uid: String,
        @Query("limit") limit: Int = 10,
        @Query("offset") offset: Int = 0
    ): Observable<FriendResponse>

    @GET("friend/{id}")
    fun getFriend(@Path("id") id: Int): Observable<Friend>

    @GET("friend/lists")
    fun searchFriend(
        @Query("name") name: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Observable<FriendResponse>

    @GET("/friend/requested-lists")
    fun getFriendRequests(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Observable<FriendRequestResponse>

    @GET("/friend/requested-count")
    fun getRequestCount(): Observable<CountResponse>

    @POST("/friend/{uid}/add")
    fun addFriend(@Path("uid") uid: String): Observable<StatusResponse>

    @PUT("/friend/{userId}/accept")
    fun acceptFriend(@Path("userId") userId: Int): Observable<StatusResponse>

    @PUT("/friend/{userId}/accept")
    fun acceptFriendRetrofit(@Path("userId") userId: Int): Call<StatusResponse>

    @DELETE("/friend/{userId}/reject")
    fun rejectFriend(@Path("userId") userId: Int): Observable<StatusResponse>

    @DELETE("/friend/{userId}/reject")
    fun rejectFriendRetrofit(@Path("userId") userId: Int): Call<StatusResponse>

    @PUT("/friend/{userId}/block")
    fun blockFriend(@Path("userId") userId: Int): Observable<StatusResponse>

    @PUT("/user/{id}/mute")
    fun muteChat(
        @Path("id") userId: Int
    ): Observable<Boolean>

    @GET("/chat/lists")
    fun getChatList(
        @Query("receiverId") receiverId: Int,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Observable<ChatResponse>

    @GET("/chat/recent-lists")
    fun getRecentChat(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Observable<InboxResponse>

    @Multipart
    @POST("upload/image")
    fun upload(
        @Part file: MultipartBody.Part
    ): Observable<UploadResponse>


    @FormUrlEncoded
    @POST("user-token")
    fun createToken(
        @Field("token") token: String
    ): Observable<StatusResponse>

    @DELETE("/user-token/{token}")
    fun deleteToken(
        @Path("token") token: String
    ): Observable<StatusResponse>

    companion object Factory {
        private const val BASE_URL = Const.BASE_URL
        private var token: String = ""

        fun create(context: Context): LetChatApiService {
            token = Pref.getAccessToken(context)

            val cacheSize = (5 * 1024 * 1024).toLong()
            val myCache = Cache(context.cacheDir, cacheSize)

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
                .cache(myCache)
                .addInterceptor { chain ->
                    var request = chain.request()
                    try {
                        request = if (hasNetwork(context))
                            request.newBuilder()
                                .header("Cache-Control", "public, max-age=" + 5)
                                .header("Accept", "application/json")
                                .header("Authorization", token)
                                .header("Content-Type", "application/json")
                                .build()
                        else
                            request.newBuilder().header(
                                "Cache-Control",
                                "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7
                            ).build()
                        chain.proceed(request)
                    } catch (ex: Exception) {
                        Timber.e(ex)
                        chain.proceed(
                            request.newBuilder().header(
                                "Cache-Control",
                                "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7
                            ).build()
                        )
                    }
                }
                .addInterceptor(interceptor)
                .build()

            val retrofit = Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()))
                .baseUrl(BASE_URL)
                .build()

            return retrofit.create(LetChatApiService::class.java)
        }

        private fun hasNetwork(context: Context): Boolean {
            var isConnected = false // Initial Value
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            if (activeNetwork != null && activeNetwork.isConnected)
                isConnected = true
            return isConnected
        }
    }
}