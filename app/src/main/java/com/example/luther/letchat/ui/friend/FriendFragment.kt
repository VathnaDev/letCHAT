package com.example.luther.letchat.ui.friend

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.service.MessageEvent
import com.example.luther.letchat.ui.chat.ChatActivity
import com.example.luther.letchat.ui.friend.list.FriendListActivity
import com.example.luther.letchat.ui.friendrequest.FriendsRequestActivity
import com.example.luther.letchat.ui.inbox.InboxActivity
import com.example.luther.letchat.ui.searchfriend.SearchFriendsActivity
import com.example.luther.letchat.utils.loadProfile
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_friend.*
import kotlinx.android.synthetic.main.recent_layout_item.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class FriendFragment : Fragment() {

    private lateinit var mViewModel: FriendFragmentViewModel

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_friend, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel = ViewModelProviders.of(activity!!)
                .get(FriendFragmentViewModel::class.java)

        registerListeners()
        observeData()
    }

    private fun registerListeners() {
        etSearch.setOnClickListener {
            startActivity(Intent(activity, SearchFriendsActivity::class.java))
        }

        btnShowAllFriendRequest.setOnClickListener {
            startActivity(Intent(activity, FriendsRequestActivity::class.java))
        }

        btnShowAllOthers.setOnClickListener {
            startActivity(Intent(activity, FriendListActivity::class.java))
        }
    }

    private fun observeData() {
        mViewModel.recentChats.observe(activity!!, Observer { chats ->
            recentLayout.removeAllViews()
            chats?.forEach { friend ->
                val view = LayoutInflater.from(activity!!).inflate(R.layout.recent_layout_item, recentLayout, false)
                view.tvName.text = friend.name
                view.findViewById<CircleImageView>(R.id.ivProfile)
                    .loadProfile("https://avatars1.githubusercontent.com/u/29161226?s=250&v=4", friend.isOnline)
                view.setOnClickListener { startChat(friend.id) }
                recentLayout.addView(view)
            }
        })

        mViewModel.others.observe(activity!!, Observer { chats ->
            othersLayout.removeAllViews()
            chats?.forEach { friend ->
                val view = LayoutInflater.from(activity!!).inflate(R.layout.friend_message_item, othersLayout, false)
                view.findViewById<TextView>(R.id.tvName).text = friend.name
                view.findViewById<TextView>(R.id.tvStatus).text =
                        if (friend.isOnline == 1) "Online" else "last seen ${DateUtils.getRelativeTimeSpanString(friend.lastOnlineAt.time)}"
                view.findViewById<CircleImageView>(R.id.ivProfile)
                        .loadProfile("https://avatars1.githubusercontent.com/u/29161226?s=250&v=4", friend.isOnline)
                view.setOnClickListener { startChat(friend.id) }
                othersLayout.addView(view)
            }
        })

        mViewModel.requests.observe(activity!!, Observer { requests ->
            friendRequestLayout.removeAllViews()
            requests?.forEach { request ->
                val view =
                        LayoutInflater.from(activity!!).inflate(R.layout.friend_request_item, friendRequestLayout, false)
                view.findViewById<TextView>(R.id.tvName).text = request.name
                view.findViewById<Button>(R.id.btnReject).setOnClickListener { mViewModel.rejectRequest(request.id) }
                view.findViewById<Button>(R.id.btnAccept).setOnClickListener { mViewModel.acceptRequest(request.id) }
                val profile = view.findViewById<CircleImageView>(R.id.ivProfile)
                profile.loadProfile("https://avatars1.githubusercontent.com/u/29161226?s=250&v=4", request.isOnline)

                friendRequestLayout.addView(view)
            }

            LetChatApp.friendRequestCount.value?.let {
                LetChatApp.friendRequestCount.postValue(requests?.size ?: 0)
            }
        })

        mViewModel.errorMessage.observe(activity!!, Observer {
            Toast.makeText(activity!!, it, Toast.LENGTH_LONG).show()
        })
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event : MessageEvent) {
        mViewModel.fetchData()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


    private fun startChat(id: Int) {
        val intent = Intent(context, ChatActivity::class.java)
        intent.putExtra(ChatActivity.RECEIVER_ID, id)
        startActivity(intent)
    }

}
