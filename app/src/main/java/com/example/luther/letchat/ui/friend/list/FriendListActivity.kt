package com.example.luther.letchat.ui.friend.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.ui.searchfriend.adapter.FriendsListAdapter
import kotlinx.android.synthetic.main.activity_friend_list.*

class FriendListActivity : AppCompatActivity() {

    private val mFriendList = ArrayList<Friend>()
    private val mFriendListAdapter = FriendsListAdapter(mFriendList,R.layout.friend_list_item)
    private lateinit var mViewModel: FriendListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friend_list)

        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mViewModel = ViewModelProviders.of(this)
            .get(FriendListViewModel::class.java)

        rvFriendList.adapter = mFriendListAdapter
        observeData()

    }

    private fun observeData() {
        mViewModel.friends.observe(this, Observer { friends ->
            friends?.let {
                mFriendList.clear()
                mFriendList.addAll(it)
                mFriendListAdapter.notifyDataSetChanged()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
