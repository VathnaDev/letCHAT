package com.example.luther.letchat.ui.setting

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.repo.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SettingFragmentViewModel(app: Application) : AndroidViewModel(app) {
    private val mDisposable = CompositeDisposable()
    private val mUserRepository = UserRepository(LetChatApiService.create(app))

    private val _allowNotification = MutableLiveData<Boolean>()
    val allowNotification: LiveData<Boolean>
        get() = _allowNotification

    fun toggleNotification() {
        mDisposable.add(
            mUserRepository.toggleNotification()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _allowNotification.postValue(it)
                }, {
                    Timber.e(it)
                })
        )
    }

    override fun onCleared() {
        if (!mDisposable.isDisposed)
            mDisposable.dispose()
        super.onCleared()
    }
}