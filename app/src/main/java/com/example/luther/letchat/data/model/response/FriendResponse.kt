package com.example.luther.letchat.data.model.response

import com.example.luther.letchat.data.model.Friend

data class FriendResponse(
    val data: List<Friend>,
    val metadata: Metadata
)