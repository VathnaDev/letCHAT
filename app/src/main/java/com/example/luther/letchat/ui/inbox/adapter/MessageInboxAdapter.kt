package com.example.luther.letchat.ui.inbox.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.Message
import com.example.luther.letchat.ui.chat.ChatActivity
import com.example.luther.letchat.utils.loadProfile
import kotlinx.android.synthetic.main.inbox_message_item.view.*
import java.text.SimpleDateFormat
import java.util.*


class MessageInboxAdapter(
    private val messages: List<Message>
) : RecyclerView.Adapter<MessageInboxAdapter.MessageInboxViewHolder>() {
    private lateinit var mContext: Context

    private val mTimeFormat = SimpleDateFormat("h:mm a", Locale.US)

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MessageInboxViewHolder {
        mContext = viewGroup.context
        val view = LayoutInflater.from(mContext).inflate(R.layout.inbox_message_item, viewGroup, false)
        return MessageInboxViewHolder(view)
    }

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: MessageInboxViewHolder, position: Int) {
        val message = messages[position]
        holder.bind(message)
    }

    inner class MessageInboxViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(message: Message) {
            if(message.seen == "unseen")
                itemView.ivMessageType.setImageDrawable(mContext.getDrawable(R.drawable.ic_message_seen))
            else
                itemView.ivMessageType.setImageDrawable(mContext.getDrawable(R.drawable.ic_message_sent))

            if (message.seen == "unseen" && message.senderId != LetChatApp.user.id) {
                val type: Typeface = Typeface.create(itemView.tvMessage.typeface, Typeface.BOLD)

                itemView.tvName.typeface = type
                itemView.tvMessage.typeface = type
                itemView.tvMessage.setTextColor(mContext.getColor(android.R.color.black))
            }else{
                val type: Typeface = Typeface.create(itemView.tvMessage.typeface, Typeface.NORMAL)

                itemView.tvName.typeface = type
                itemView.tvMessage.typeface = type
            }

            itemView.tvMessage.text = if (message.content == "") "${message.name} sent you a photo" else message.content
            itemView.tvName.text = message.name
            itemView.tvMessageDate.text =
                    if (DateUtils.isToday(message.sentAt.time)) mTimeFormat.format(message.sentAt) else DateUtils.getRelativeDateTimeString(
                        itemView.context,
                        message.sentAt.time,
                        DateUtils.MINUTE_IN_MILLIS,
                        DateUtils.WEEK_IN_MILLIS,
                        0
                    )
            itemView.inboxItemLayout.setOnClickListener {
                val intent = Intent(mContext, ChatActivity::class.java)
                intent.putExtra(ChatActivity.RECEIVER_ID, message.id)
                mContext.startActivity(intent)
            }
        }
    }
}