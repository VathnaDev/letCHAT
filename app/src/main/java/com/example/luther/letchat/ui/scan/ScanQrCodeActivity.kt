package com.example.luther.letchat.ui.scan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.luther.letchat.R
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import android.app.Activity
import android.content.Intent


class ScanQrCodeActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    private lateinit var mScannerView: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_qr_code)
    }

    override fun onStart() {
        super.onStart()
        
        mScannerView = ZXingScannerView(this)
        setContentView(mScannerView)
        mScannerView.setResultHandler(this)
        mScannerView.startCamera()
    }

    override fun handleResult(result: Result) {
        val returnIntent = Intent()
        returnIntent.putExtra("result", result.text)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
    }

}
