package com.example.luther.letchat.data.model

import java.util.*

data class User(
    val createdAt: Date,
    val email: String,
    val facebookId: String,
    val id: Int,
    val image: Any,
    val isOnline: Boolean,
    val lastOnlineAt: Date,
    val name: String,
    val password: String,
    val status: String,
    val uid: String,
    val updatedAt: Date,
    val useNotification: Boolean
)