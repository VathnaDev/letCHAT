package com.example.luther.letchat

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import com.example.luther.letchat.data.model.User
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.utils.Const
import com.github.nkzawa.socketio.client.Socket
import timber.log.Timber
import java.lang.Exception
import com.github.nkzawa.socketio.client.Manager
import java.net.URI

class LetChatApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
    companion object {
        lateinit var user: User
        lateinit var mSocket: Socket
        var receiverId = 0
        var unSeenMessageCount: MutableLiveData<Int> = MutableLiveData()
        var friendRequestCount: MutableLiveData<Int> = MutableLiveData()

        fun setupSocket(accessToken: String) {
            try {
                val opts = Manager.Options()
                opts.query = "token=${accessToken.replace("Bearer ", "")}"

                val manager = Manager(URI(Const.BASE_URL), opts)
                mSocket = manager.socket("/chat-app")
                mSocket.connect()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

}