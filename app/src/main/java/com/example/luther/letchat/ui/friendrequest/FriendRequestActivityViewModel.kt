package com.example.luther.letchat.ui.friendrequest

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.FriendRequest
import com.example.luther.letchat.data.repo.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class FriendRequestActivityViewModel(app: Application) : AndroidViewModel(app) {
    private val mDisposable = CompositeDisposable()
    private val mUserRepo = UserRepository(LetChatApiService.create(app))

    init {
        fetchRequests()
    }

    private val _requestList = MutableLiveData<List<FriendRequest>>()
    val requests: LiveData<List<FriendRequest>>
        get() = _requestList

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage


    private fun fetchRequests() {
        mDisposable.add(
            mUserRepo.getFriendRequest()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _requestList.postValue(it.data)
                }, {
                    Timber.e(it)
                })
        )
    }

    fun rejectRequest(id: Int) {
        mDisposable.add(
            mUserRepo.rejectRequest(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Timber.d(response.toString())
                    response.statusCode?.let {
                        _errorMessage.postValue(response.message)
                    }
                }, {
                    Timber.e(it)
                    _errorMessage.postValue(it.message)
                })

        )
    }

    fun acceptRequest(id: Int) {
        mDisposable.add(
            mUserRepo.acceptRequest(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Timber.d(response.toString())
                    response.statusCode?.let {
                        _errorMessage.postValue(response.message)
                    }
                }, {
                    Timber.e(it)
                    _errorMessage.postValue(it.message)
                })
        )
    }

    override fun onCleared() {
        if (!mDisposable.isDisposed)
            mDisposable.dispose()
        super.onCleared()
    }
}