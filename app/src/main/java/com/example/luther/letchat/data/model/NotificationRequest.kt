package com.example.luther.letchat.data.model

import java.util.*


data class NotificationRequest(
    val id: Int,
    val image: String,
    val isOnline: Boolean,
    val lastOnlineAt: Date,
    val name: String,
    val screenType: String,
    val uid: String,
    val content: String,
    val gallery : String,
    val senderId : Int,
    val receiverId: Int
)