package com.example.luther.letchat.data.model.response

import com.example.luther.letchat.data.model.Message

data class InboxResponse(
    val data: List<Message>,
    val metadata: Metadata
)