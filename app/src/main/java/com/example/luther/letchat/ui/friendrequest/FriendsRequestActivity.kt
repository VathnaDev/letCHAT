package com.example.luther.letchat.ui.friendrequest

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.FriendRequest
import kotlinx.android.synthetic.main.activity_friends_request.*

class FriendsRequestActivity : AppCompatActivity(), FriendRequestListAdapter.FriendRequestItemListener {

    private val mRequests = arrayListOf<FriendRequest>()
    private val mAdapter = FriendRequestListAdapter(mRequests, this)
    private lateinit var mViewModel: FriendRequestActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends_request)

        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mViewModel = ViewModelProviders.of(this)
            .get(FriendRequestActivityViewModel::class.java)

        rvFriendRequest.adapter = mAdapter

        observeData()
    }

    private fun observeData() {
        mViewModel.requests.observe(this, Observer {
            mRequests.clear()
            mRequests.addAll(it ?: arrayListOf())
            mAdapter.notifyDataSetChanged()
        })

        mViewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onReject(id: Int) {
        mViewModel.rejectRequest(id)
    }

    override fun onAccept(id: Int) {
        mViewModel.acceptRequest(id)
    }
}
