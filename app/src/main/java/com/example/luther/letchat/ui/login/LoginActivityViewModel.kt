package com.example.luther.letchat.ui.login

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.User
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.data.repo.UserRepository
import com.example.luther.letchat.utils.Const
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class LoginActivityViewModel(private val app: Application) : AndroidViewModel(app) {
    private val disposable = CompositeDisposable()
    private val userRepository = UserRepository(LetChatApiService.create(app))

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private val _failLoginWithFacebook = MutableLiveData<String>()
    val failLoginWithFacebook: LiveData<String>
        get() = _failLoginWithFacebook

    private var _user = MutableLiveData<User>()
    val currentUser: LiveData<User>
        get() = _user

    fun loginUser(email: String, password: String) {
        disposable.add(
            userRepository.loginUser(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Pref.getEditor(app).putString(
                        Const.ACCESS_TOKEN,
                        response.accessToken
                    ).commit().also {
                        _user.postValue(response.user)
                    }
                }, {
                    Timber.e(it)
                    _errorMessage.postValue(it.message)
                })
        )
    }
//    private var _errorMessage = MutableLiveData<ApiError>()
//    val errorMessage: LiveData<ApiError>
//        get() = _errorMessage


    fun loginWithFacebook(tokenKey: String) {
        disposable.add(
            userRepository.loginWithFacebook(tokenKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        Pref.getEditor(getApplication()).putString(
                            Const.ACCESS_TOKEN,
                            it.accessToken
                        ).commit()
                        _user.postValue(it.user)
                    },
                    {
                        Timber.e(it)
                        _failLoginWithFacebook.postValue(it.message)
                    })
        )
    }

    override fun onCleared() {
        if (!disposable.isDisposed)
            disposable.dispose()
        super.onCleared()
    }

}