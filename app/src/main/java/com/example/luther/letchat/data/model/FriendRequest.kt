package com.example.luther.letchat.data.model

import com.google.gson.annotations.SerializedName

data class FriendRequest(
    val id: Int,
    val image: Any,
    @SerializedName("is_online")
    val isOnline: Int,
    @SerializedName("last_online_at")
    val lastOnlineAt: String,
    val name: String,
    val uid: String
)