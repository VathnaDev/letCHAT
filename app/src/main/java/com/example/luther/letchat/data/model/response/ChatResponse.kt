package com.example.luther.letchat.data.model.response

import com.example.luther.letchat.data.model.Message

data class ChatResponse(
    val uid: String,
    val chats: List<Message>,
    val metadata: Metadata
)