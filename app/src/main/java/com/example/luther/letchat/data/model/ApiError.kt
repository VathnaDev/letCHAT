package com.example.luther.letchat.data.model

data class ApiError(val statusCode: Int, val message: String)