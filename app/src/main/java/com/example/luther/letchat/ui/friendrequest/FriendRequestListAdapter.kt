package com.example.luther.letchat.ui.friendrequest

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.FriendRequest
import com.example.luther.letchat.utils.loadProfile
import kotlinx.android.synthetic.main.friend_request_item.view.*

class FriendRequestListAdapter(
    private val mRequests: List<FriendRequest>,
    private val mFriendRequestItemListener: FriendRequestItemListener
) :
    RecyclerView.Adapter<FriendRequestListAdapter.FriendRequestListViewHolder>() {
    private lateinit var context: Context

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): FriendRequestListViewHolder {
        context = viewGroup.context
        val view = LayoutInflater.from(context).inflate(R.layout.friend_request_item, viewGroup, false)
        return FriendRequestListViewHolder(view)
    }

    override fun getItemCount(): Int = mRequests.size

    override fun onBindViewHolder(holder: FriendRequestListViewHolder, position: Int) {
        val request = mRequests[position]
        holder.bind(request)
    }


    inner class FriendRequestListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(request: FriendRequest) {
            itemView.tvName.text = request.name
            itemView.ivProfile.loadProfile("https://avatars1.githubusercontent.com/u/29161226?s=250&v=4", 0)
            itemView.btnAccept.setOnClickListener { mFriendRequestItemListener.onAccept(request.id) }
            itemView.btnReject.setOnClickListener { mFriendRequestItemListener.onReject(request.id) }
        }
    }

    interface FriendRequestItemListener {
        fun onReject(id: Int)
        fun onAccept(id: Int)
    }
}