package com.example.luther.letchat.data.repo

import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.data.model.User
import com.example.luther.letchat.data.model.response.*
import io.reactivex.Observable
import okhttp3.MultipartBody

class UserRepository(private val api: LetChatApiService) {

    fun registerUser(
        name: String,
        email: String,
        uid: String,
        password: String
    ): Observable<UserResponse> {
        return api.registerUser(name, email, uid, password)
    }

    fun registerUserWithFacebook(
        name: String,
        email: String,
        uid: String,
        tokenKey: String
    ): Observable<UserResponse> {
        return api.registerUserWithFacebook(name, email, uid, tokenKey)
    }

    fun searchUser(uid: String): Observable<FriendResponse> {
        return api.searchUser(uid)
    }

    fun searchFriend(name: String,limit: Int = 10,offset: Int = 0): Observable<FriendResponse> {
        return api.searchFriend(name,limit,offset)
    }

    fun loginUser(email: String, password: String): Observable<UserResponse> {
        return api.loginUser(email, password)
    }

    fun loginWithFacebook(token: String): Observable<UserResponse> {
        return api.loginWithFacebook(token)
    }

    fun addFriend(uid: String): Observable<StatusResponse> {
        return api.addFriend(uid)
    }

    fun getCurrentUser(): Observable<User> {
        return api.getCurrentUser()
    }

    fun toggleNotification(): Observable<Boolean> {
        return api.toggleNotification()
    }

    fun getRecentChatInbox(limit: Int = 10, offset: Int = 0): Observable<InboxResponse> {
        return api.getRecentChat(limit, offset)
    }

    fun getFriendRequest(limit: Int = 10, offset: Int = 0): Observable<FriendRequestResponse> {
        return api.getFriendRequests(limit, offset)
    }

    fun rejectRequest(id: Int): Observable<StatusResponse> {
        return api.rejectFriend(id)
    }

    fun acceptRequest(id: Int): Observable<StatusResponse> {
        return api.acceptFriend(id)
    }

    fun getRequestCount(): Observable<CountResponse> {
        return api.getRequestCount()
    }

    fun getFriend(id: Int): Observable<Friend> {
        return api.getFriend(id)
    }

    fun getChatList(receiverId: Int, limit: Int = 100, offset: Int = 0): Observable<ChatResponse> {
        return api.getChatList(receiverId, limit, offset)
    }

    fun uploadImage(image: MultipartBody.Part): Observable<UploadResponse> {
        return api.upload(image)
    }

    fun muteChat(userId: Int): Observable<Boolean> {
        return api.muteChat(userId)
    }

    fun blockUser(userId: Int) : Observable<StatusResponse> {
        return api.blockFriend(userId)
    }

    fun createToken(token: String): Observable<StatusResponse> {
        return api.createToken(token)
    }


}