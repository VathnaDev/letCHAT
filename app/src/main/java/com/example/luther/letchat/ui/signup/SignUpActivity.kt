package com.example.luther.letchat.ui.signup

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.ui.login.LoginActivity
import com.example.luther.letchat.ui.main.MainActivity
import com.example.luther.letchat.utils.ActionAlertDialogUtils
import com.example.luther.letchat.utils.ErrorDialogUtils
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var mViewModel: SignUpActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        setSupportActionBar(toolbar3)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = null

        mViewModel = ViewModelProviders.of(this)
                .get(SignUpActivityViewModel::class.java)
        progressBar?.visibility = View.INVISIBLE

        setupListener()
        observeData()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (item.itemId == android.R.id.home) {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //    override fun onBackPressed() {
//        val alertMessage = "Do you want to exit this activity?"
//        ActionAlertDialogUtils.performActionAlertDialog(this, alertMessage,object : ActionAlertDialogUtils.ActionResultListener{
//            override fun onResultYes() {
//                onBackPressed()
//            }
//
//        })
//    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            val alertMessage = "Do you want to exit this activity?"
            ActionAlertDialogUtils.performActionAlertDialog(this, alertMessage, object : ActionAlertDialogUtils.ActionResultListener {
                override fun onResultYes() {
                    onBackPressed()
                }
            })
            false
        } else
            super.onKeyDown(keyCode, event)
    }

    private fun setupListener() {
        btnSignUp.setOnClickListener {
            val name: String = etName.text.toString()
            val id: String = etId.text.toString()
            val email: String = etEmail.text.toString()
            val password: String = etPassword.text.toString()
            //Data validation
            if (name.isEmpty() || id.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches() || password.length < 6) {
                if (name.isEmpty()) {
                    etName.error = "Username cannot be blank!"
                }
                if (id.isEmpty()) {
                    etId.error = "User id cannot be blank!"
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    etEmail.error = "Email cannot be blank and must follow the standard form of email address!"
                }
                if (password.length < 6) {
                    etPassword.error = "password cannot be blank or less than 6 characters!"
                }
                return@setOnClickListener
            }
            performSignUp(name, id, email, password)
        }
    }


    fun observeData() {
        mViewModel.user.observe(this, Observer { user ->
            user?.let {
                progressBar?.visibility = View.INVISIBLE
                LetChatApp.user = it
                startActivity(Intent(this, MainActivity::class.java))
                finishAffinity()
            }
        })

        mViewModel.errorMessage.observe(this, Observer {
            progressBar?.visibility = View.INVISIBLE
            val errorResponse = when {
                it.toString().contains("400") -> "Email or user id already exist."
                it.toString().contains("502") -> "Weak connection!"
                else -> "Something went wrong!"
            }
            ErrorDialogUtils.performErrorMessageDialog(this, errorResponse)
        })
    }

    private fun performSignUp(name: String, id: String, email: String, password: String) {
        progressBar?.visibility = View.VISIBLE
        mViewModel.registerUser(name, id, email, password)
    }
}
