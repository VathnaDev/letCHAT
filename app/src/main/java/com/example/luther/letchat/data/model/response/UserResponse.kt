package com.example.luther.letchat.data.model.response

import com.example.luther.letchat.data.model.User
import com.google.gson.annotations.SerializedName

data class UserResponse(
    val accessToken: String,
    val user: User,
    val statusCode : Int,
    val message : String
)