package com.example.luther.letchat.data.model.response

import com.example.luther.letchat.data.model.Image

data class UploadResponse(
    val image: Image
)