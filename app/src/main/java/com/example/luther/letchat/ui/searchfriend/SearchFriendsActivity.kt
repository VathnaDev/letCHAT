package com.example.luther.letchat.ui.searchfriend

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.ui.searchfriend.adapter.FriendsListAdapter
import kotlinx.android.synthetic.main.activity_search_friends.*

class SearchFriendsActivity : AppCompatActivity() {

    private val mFriendsList = arrayListOf<Friend>()

    private lateinit var mFriendsAdapter: FriendsListAdapter
    private lateinit var mViewModel: SearchFriendActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_friends)

        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mViewModel = ViewModelProviders.of(this)
            .get(SearchFriendActivityViewModel::class.java)

        mFriendsAdapter = FriendsListAdapter(mFriendsList)
        rvSearchList.adapter = mFriendsAdapter

        initListeners()
        observeData()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun initListeners() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mViewModel.searchFriend(s.toString())
            }

        })
    }

    private fun observeData() {
        mViewModel.friends.observe(this, Observer { friends ->
            friends?.let {
                mFriendsList.clear()
                mFriendsList.addAll(it)
                mFriendsAdapter.notifyDataSetChanged()
            }
        })
    }


}
