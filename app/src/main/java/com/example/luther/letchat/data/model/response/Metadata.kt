package com.example.luther.letchat.data.model.response

data class Metadata(
    val count: Int = 0,
    val limit: Int = 10,
    val offset: Int = 0
)