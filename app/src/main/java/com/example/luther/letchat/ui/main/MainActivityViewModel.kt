package com.example.luther.letchat.ui.main

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.widget.Toast
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.response.StatusResponse
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.data.repo.UserRepository
import com.example.luther.letchat.utils.Const
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.Socket
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class MainActivityViewModel(private val app: Application) : AndroidViewModel(app) {
    private val mDisposable = CompositeDisposable()
    private val mUserRepository = UserRepository(LetChatApiService.create(app))

    private var mSocket: Socket = LetChatApp.mSocket

    private val onErrorListener = Emitter.Listener { errors ->
        errors.forEach {
            Timber.d(it.toString())
        }
    }

    init {
        fetchRequestCount()
        mSocket.on("add-friend") { request ->
            LetChatApp.friendRequestCount.value?.let {
                LetChatApp.friendRequestCount.postValue(it + 1)
            }
        }.on("exception", onErrorListener)
    }

    fun sendFriendRequest(uid: String): LiveData<StatusResponse> {
        val response = MutableLiveData<StatusResponse>()
        mDisposable.add(
            mUserRepository.addFriend(uid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    response.postValue(StatusResponse(it.message, it.statusCode))
                }, {
                    Toast.makeText(app, "Friend request already sent", Toast.LENGTH_SHORT).show()

                })
        )
        return response
    }

    fun getCurrentUser() {
        mDisposable.add(mUserRepository.getCurrentUser()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    LetChatApp.user = response
                },
                {
                    Timber.e(it)
                }
            )
        )
    }

    private fun fetchRequestCount() {
        mDisposable.add(mUserRepository.getRequestCount()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                LetChatApp.friendRequestCount.postValue(it.count)
            }
        )
    }

    fun createFcmToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { firebase ->
            mDisposable.add(
                mUserRepository.createToken(firebase.token)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        Pref.getEditor(app)
                            .putString(Const.FIREBASE_TOKEN, firebase.token)
                            .commit()
                        Timber.d(it.message)
                    }, {
                        Timber.e(it)
                    })
            )
        }
    }


    override fun onCleared() {
        if (!mDisposable.isDisposed)
            mDisposable.dispose()
        super.onCleared()
    }


}