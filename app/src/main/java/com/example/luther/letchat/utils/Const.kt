package com.example.luther.letchat.utils

object Const {
    const val BASE_URL = "http://192.168.19.71:8082/"
//    const val BASE_URL = "https://let-chat-pm4.herokuapp.com/"
    const val ACCESS_TOKEN = "access_token"
    const val FIREBASE_TOKEN = "firebase_token"
    const val ALLOW_NOTIFICATION = "allow_notification"
    const val PERMISSION_CAMERA_REQUEST_CODE = 1
    const val PERMISSION_READ_STORAGE_REQUEST_CODE = 2

}