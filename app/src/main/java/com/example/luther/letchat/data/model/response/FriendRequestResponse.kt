package com.example.luther.letchat.data.model.response

import com.example.luther.letchat.data.model.FriendRequest


data class FriendRequestResponse(
    val data: List<FriendRequest>,
    val metadata: Metadata
)