package com.example.luther.letchat.ui.searchfriend

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.widget.Toast
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.data.repo.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SearchFriendActivityViewModel(app: Application) : AndroidViewModel(app) {
    private val mDisposable: CompositeDisposable = CompositeDisposable()
    private var mUserRepository: UserRepository = UserRepository(LetChatApiService.create(app))

    private val _friendList = MutableLiveData<List<Friend>>()
    val friends: LiveData<List<Friend>>
        get() = _friendList

    fun searchFriend(name: String) {
        mDisposable.add(
            mUserRepository.searchFriend(name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d(it.toString())
                    _friendList.postValue(it.data)
                }, { error ->
                    Toast.makeText(getApplication(), error.message, Toast.LENGTH_SHORT).show()
                })
        )
    }

    override fun onCleared() {
        if (!mDisposable.isDisposed) {
            mDisposable.dispose()
        }
        super.onCleared()
    }
}