package com.example.luther.letchat.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import android.support.v4.app.RemoteInput
import com.bumptech.glide.Glide
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.NotificationRequest
import com.example.luther.letchat.ui.chat.ChatActivity
import com.example.luther.letchat.ui.friend.list.FriendListActivity
import com.example.luther.letchat.ui.friendrequest.FriendsRequestActivity
import com.example.luther.letchat.utils.Const


object AppNotification {
    const val KEY_TEXT_REPLY = "key_text_reply"
    const val KEY_NOTIFICATION_ID = "key_notification_id"
    const val SENDER_ID = "sender_id"
    const val RECEIVER_ID = "receiver_id"

    lateinit var mNotifyManager: NotificationManager

    private lateinit var notificationChannel: NotificationChannel
    private const val PRIMARY_CHANNEL_ID = "letChatChannel"

    fun pushChatNotification(
        context: Context,
        request: NotificationRequest
    ) {
        val notificationId = System.currentTimeMillis().toInt()

        val chatIntent = Intent(context, ChatActivity::class.java)
        chatIntent.putExtra(ChatActivity.RECEIVER_ID, request.senderId)
        val notificationBuilder =
            getChatNotificationBuilder(
                context,
                chatIntent,
                notificationId,
                request
            )

        if (request.gallery.isNotEmpty()) {
            val bitmap = Glide.with(context)
                .asBitmap()
                .load(Const.BASE_URL + request.gallery)
                .submit(500, 500)
                .get()
            notificationBuilder.setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap))
        }

        mNotifyManager.notify(notificationId, notificationBuilder.build())
    }

    private fun getChatNotificationBuilder(
        context: Context,
        notificationIntent: Intent,
        notificationId: Int,
        request: NotificationRequest
    ): NotificationCompat.Builder {
        mNotifyManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        //Android version O or above required NotificationChannel
        if (android.os.Build.VERSION.SDK_INT >=
            android.os.Build.VERSION_CODES.O
        ) {
            notificationChannel = NotificationChannel(
                PRIMARY_CHANNEL_ID,
                "Notification",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.enableVibration(true)
            notificationChannel.description = "letCHAT App Notification"
            mNotifyManager.createNotificationChannel(notificationChannel)
        }

        //Pending Intent for user click on the notification
        //It will open the ChatActivity
        notificationIntent.putExtra(KEY_NOTIFICATION_ID, notificationId)
        val notificationPendingIntent = PendingIntent.getActivity(
            context,
            notificationId,
            notificationIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )


        //Pending Intent for user click on the Reply and Send button on notification
        //It will send broadcast to DirectReplyReceiver
        val directReplyIntent = Intent(context, DirectReplyReceiver::class.java)
        directReplyIntent.putExtra(KEY_NOTIFICATION_ID, notificationId)
        //Switch From receiver to sender
        directReplyIntent.putExtra(RECEIVER_ID, request.senderId)
        directReplyIntent.putExtra(SENDER_ID, request.receiverId)

        val directReplyPendingIntent = PendingIntent.getBroadcast(
            context,
            notificationId,
            directReplyIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        //Create remote input which will attached to the notification action
        val remoteInput: RemoteInput = RemoteInput.Builder(KEY_TEXT_REPLY).run {
            setLabel("REPLY")
            build()
        }

        //Create Reply Action for sending message from notification
        val action = NotificationCompat.Action.Builder(
            R.drawable.emoji_recent,
            "REPLY",
            directReplyPendingIntent
        )
            .addRemoteInput(remoteInput)
            .build()

        return getDefaultNotificationBuilder(context)
            .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.profile))
            .setContentTitle("Message")
            .setContentText(if (request.content.isEmpty()) "${request.name} sent you a photo" else request.content)
            .setContentIntent(notificationPendingIntent)
            .addAction(action)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
    }


    fun pushFriendRequestNotification(context: Context, request: NotificationRequest) {
        mNotifyManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notificationId = System.currentTimeMillis().toInt()

        val friendRequestListIntent = PendingIntent.getActivity(
            context,
            request.id,
            Intent(context, FriendsRequestActivity::class.java),
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        //Actions
        val acceptRequestIntent = Intent(context, FriendRequestActionReceiver::class.java).apply {
            putExtra(FriendRequestActionReceiver.ACTION_TYPE, FriendRequestActionReceiver.ACCEPT)
            putExtra(FriendRequestActionReceiver.USER_ID, request.id)
            putExtra(FriendRequestActionReceiver.NOTIFICATION_ID, notificationId)
        }

        val rejectRequestIntent = Intent(context, FriendRequestActionReceiver::class.java).apply {
            putExtra(FriendRequestActionReceiver.ACTION_TYPE, FriendRequestActionReceiver.REJECT)
            putExtra(FriendRequestActionReceiver.USER_ID, request.id)
            putExtra(FriendRequestActionReceiver.NOTIFICATION_ID, notificationId)
        }

        val acceptPendingIntent = PendingIntent.getBroadcast(
            context,
            System.nanoTime().toInt(),
            acceptRequestIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val rejectPendingIntent = PendingIntent.getBroadcast(
            context,
            System.nanoTime().toInt(),
            rejectRequestIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )


        val notification = getDefaultNotificationBuilder(context)
            .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.profile))
            .setContentTitle("Friend Request")
            .setContentText("${request.name} sent you a request")
            .setContentIntent(friendRequestListIntent)
            .addAction(R.drawable.ic_block, "ACCEPT", acceptPendingIntent)
            .addAction(R.drawable.ic_block, "REJECT", rejectPendingIntent)
            .build()

        mNotifyManager.notify(notificationId, notification)
    }

    fun pushAcceptFriendRequestNotification(context: Context, request: NotificationRequest) {
        mNotifyManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notificationId = System.currentTimeMillis().toInt()

        val friendListIntent = PendingIntent.getActivity(
            context,
            request.id,
            Intent(context, FriendListActivity::class.java),
            PendingIntent.FLAG_UPDATE_CURRENT
        )


        val notification = getDefaultNotificationBuilder(context)
            .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.profile))
            .setContentTitle("Accept Request")
            .setContentText("${request.name} accept your request")
            .setContentIntent(friendListIntent)
            .build()

        mNotifyManager.notify(notificationId, notification)
    }

    private fun getDefaultNotificationBuilder(context: Context): NotificationCompat.Builder {
        return NotificationCompat.Builder(context, PRIMARY_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_send)
            .setShowWhen(true)
            .setAutoCancel(true)
    }


}