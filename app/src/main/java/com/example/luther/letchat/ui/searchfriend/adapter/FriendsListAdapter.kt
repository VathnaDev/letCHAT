package com.example.luther.letchat.ui.searchfriend.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.ui.chat.ChatActivity
import com.example.luther.letchat.utils.loadProfile
import kotlinx.android.synthetic.main.friend_message_item.view.*

class FriendsListAdapter(
        private val mFriends: List<Friend>,
        private val mResourceLayout: Int = R.layout.friend_message_item
) : RecyclerView.Adapter<FriendsListAdapter.FriendsListViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FriendsListViewHolder {
        context = p0.context
        val view = LayoutInflater.from(p0.context).inflate(mResourceLayout, p0, false)
        return FriendsListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mFriends.size
    }

    override fun onBindViewHolder(p0: FriendsListViewHolder, p1: Int) {
        val friend = mFriends[p1]
        p0.bind(friend)
    }

    inner class FriendsListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(friend: Friend) {
            itemView.tvName.text = friend.name
            itemView.ivProfile.loadProfile(
                    "https://avatars1.githubusercontent.com/u/29161226?s=250&v=4",
                    friend.isOnline
            )
            itemView.tvStatus.text =
                    if (friend.isOnline == 1) "Online" else "last seen ${DateUtils.getRelativeTimeSpanString(friend.lastOnlineAt.time)}"

            itemView.setOnClickListener {
                val intent = Intent(context, ChatActivity::class.java)
                intent.putExtra(ChatActivity.RECEIVER_ID, friend.id)
                context.startActivity(intent)
            }
        }
    }
}


