package com.example.luther.letchat.ui.signup.SignUpWithFacebook

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.User
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.data.repo.UserRepository
import com.example.luther.letchat.utils.Const
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SignUpWithFacebookActivityViewModel(application: Application): AndroidViewModel(application){
    private val userRepository = UserRepository(LetChatApiService.create(application))
    private val mDisposable = CompositeDisposable()

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private var _user = MutableLiveData<User>()
    val user: MutableLiveData<User>
        get() = _user

    fun registerUserWithFacebook(name: String, email:String, id: String, tokenKey: String) {
        mDisposable.add(
        userRepository.registerUserWithFacebook(name, email, id, tokenKey )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                Timber.d(response.toString())
                Pref.getEditor(getApplication())
                    .putString(Const.ACCESS_TOKEN, response.accessToken)
                    .commit().also {
                        _user.postValue(response.user)
                    }
            }, {
                _errorMessage.postValue(it.message)
                Timber.e(it)
            })
        )
    }

    override fun onCleared() {
        if (!mDisposable.isDisposed)
            mDisposable.dispose()
        super.onCleared()
    }
}