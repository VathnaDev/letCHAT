package com.example.luther.letchat.ui.signup

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.User
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.data.repo.UserRepository
import com.example.luther.letchat.utils.Const
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SignUpActivityViewModel(application: Application) : AndroidViewModel(application) {
    private val mDisposable = CompositeDisposable()
    private val userRepository = UserRepository(LetChatApiService.create(application))

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private var _user = MutableLiveData<User>()
    val user: LiveData<User>
        get() = _user

    fun registerUser(name: String, id: String, email: String, password: String) {
        mDisposable.add(
            userRepository.registerUser(name, email, id, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d(it.toString())
                    Pref.getEditor(getApplication())
                        .putString(Const.ACCESS_TOKEN, it.accessToken)
                        .commit()
                    _user.postValue(it.user)

                }, {
                    _errorMessage.postValue(it.message)
                    Timber.e(it)
                })
        )
    }

    override fun onCleared() {
        if (!mDisposable.isDisposed)
            mDisposable.dispose()
        super.onCleared()
    }
}