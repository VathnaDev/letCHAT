package com.example.luther.letchat.service

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.response.StatusResponse
import com.example.luther.letchat.ui.main.MainActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import org.greenrobot.eventbus.EventBus



class FriendRequestActionReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.d(intent.toString())
        if (context != null && intent != null) {
            val api = LetChatApiService.create(context)
            val type = intent.getStringExtra(ACTION_TYPE)
            val userId = intent.getIntExtra(USER_ID, 0)
            val notificationId = intent.getIntExtra(NOTIFICATION_ID, 0)

            if (type == ACCEPT) {
                api.acceptFriendRetrofit(userId)
                    .enqueue(object : Callback<StatusResponse> {
                        override fun onFailure(call: Call<StatusResponse>, t: Throwable) {
                            Timber.e(t)
                            Toast.makeText(context, "Error While Accepting Friend Request!", Toast.LENGTH_SHORT)
                                .show()
                        }

                        override fun onResponse(call: Call<StatusResponse>, response: Response<StatusResponse>) {
                            Timber.d(response.body()?.toString())
                            if (response.isSuccessful) {
                                if (response.body()?.message?.toLowerCase() == "ok") {
                                    Toast.makeText(context, "Successfully Accept Friend Request!", Toast.LENGTH_SHORT)
                                        .show()
                                    AppNotification.mNotifyManager.cancel(notificationId)
                                    if(MainActivity.isRunning)
                                        EventBus.getDefault().post(MessageEvent())
                                }
                            }
                        }
                    })
            } else {
                api.rejectFriendRetrofit(userId)
                    .enqueue(object : Callback<StatusResponse> {
                        override fun onFailure(call: Call<StatusResponse>, t: Throwable) {
                            Timber.e(t)
                            Toast.makeText(context, "Error While Reject Friend Request!", Toast.LENGTH_SHORT)
                                .show()
                        }

                        override fun onResponse(call: Call<StatusResponse>, response: Response<StatusResponse>) {
                            Timber.d(response.body()?.toString())
                            if (response.isSuccessful) {
                                if (response.body()?.message?.toLowerCase() == "ok") {
                                    Toast.makeText(context, "Successfully Reject Friend Request!", Toast.LENGTH_SHORT)
                                        .show()

                                    val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                    manager.cancel(notificationId)
                                    if(MainActivity.isRunning)
                                        EventBus.getDefault().post(MessageEvent())
                                }
                            }
                        }
                    })
            }
        }
    }


    companion object {
        const val ACTION_TYPE = "type"
        const val ACCEPT = "accept"
        const val REJECT = "reject"
        const val USER_ID = "userId"
        const val NOTIFICATION_ID = "notificationId"

    }
}