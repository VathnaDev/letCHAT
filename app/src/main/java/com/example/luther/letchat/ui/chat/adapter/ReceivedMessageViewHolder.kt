package com.example.luther.letchat.ui.chat.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.luther.letchat.data.model.Message
import com.example.luther.letchat.utils.Const
import kotlinx.android.synthetic.main.message_list_receive_item.view.*

class ReceivedMessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(message: Message) {
        //Check if message is Text or Image
        if (message.content.isEmpty()) {
            itemView.rootMessage.visibility = View.GONE
            itemView.ivMessage.visibility = View.VISIBLE
            if (message.gallery.contains("upload/chat"))
                Glide.with(itemView.context)
                    .load(Const.BASE_URL + message.gallery)
                    .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(32)))
                    .into(itemView.ivMessage)
            else
                Glide.with(itemView.context)
                    .load(message.gallery)
                    .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(32)))
                    .into(itemView.ivMessage)
        } else {
            itemView.ivMessage.visibility = View.GONE
            itemView.rootMessage.visibility = View.VISIBLE
            itemView.tvMessage.text = message.content
        }


    }
}
