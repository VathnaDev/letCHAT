package com.example.luther.letchat.ui.friend

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.data.model.FriendRequest
import com.example.luther.letchat.data.model.Message
import com.example.luther.letchat.data.repo.UserRepository
import com.example.luther.letchat.service.MessageEvent
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import timber.log.Timber

class FriendFragmentViewModel(application: Application) : AndroidViewModel(application) {
    private var mUserRepository: UserRepository = UserRepository(LetChatApiService.create(application))
    private var mSocket: Socket = LetChatApp.mSocket

    private val mGson = Gson()
    private val mDisposable = CompositeDisposable()
    private val mRecentFriend = MutableLiveData<List<Friend>>()
    private val mOthersFriend = MutableLiveData<List<Friend>>()
    private val mRequests = MutableLiveData<List<FriendRequest>>()
    private val mErrorMessage = MutableLiveData<String>()

    val recentChats: LiveData<List<Friend>>
        get() = mRecentFriend

    val others: LiveData<List<Friend>>
        get() = mOthersFriend

    val requests: LiveData<List<FriendRequest>>
        get() = mRequests

    val errorMessage: LiveData<String>
        get() = mErrorMessage

    private val onErrorListener = Emitter.Listener { errors ->
        errors.forEach {
            Timber.d(it.toString())
        }
    }

    init {
        fetchData()
        mSocket.on("add-friend") { friendRequests ->
            val friendRequest = mGson.fromJson(friendRequests[0].toString(), FriendRequest::class.java)
            mRequests.value?.let {
                (it as ArrayList<FriendRequest>).add(friendRequest)
                mRequests.postValue(it)
            }
        }.on("exception", onErrorListener)
    }

    fun fetchData() {
        fetchMessages()
        fetchRequest()
    }

    private fun fetchMessages() {
        mDisposable.add(
            mUserRepository.searchFriend("", 6, 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val recent = arrayListOf<Friend>()
                    val other = arrayListOf<Friend>()
                    it.data.forEachIndexed { index, response ->
                        if (index <= 2)
                            recent.add(response)
                        else
                            other.add(response)
                    }
                    mRecentFriend.postValue(recent)
                    mOthersFriend.postValue(other)
                }, {
                    mErrorMessage.postValue(it.message)
                    Timber.e(it)
                })
        )
    }

    private fun fetchRequest() {
        mDisposable.add(
            mUserRepository.getFriendRequest(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mRequests.postValue(it.data)
                }, {
                    mErrorMessage.postValue(it.message)
                    Timber.e(it)
                })
        )
    }

    fun rejectRequest(id: Int) {
        mDisposable.add(
            mUserRepository.rejectRequest(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Timber.d(response.toString())
                    response.statusCode?.let {
                        mErrorMessage.postValue(response.message)
                    }
                    fetchData()
                }, {
                    Timber.e(it)
                    mErrorMessage.postValue(it.message)
                })

        )
    }

    fun acceptRequest(id: Int) {
        mDisposable.add(
            mUserRepository.acceptRequest(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Timber.d(response.toString())
                    response.statusCode?.let {
                        mErrorMessage.postValue(response.message)
                    }
                    fetchData()
                }, {
                    Timber.e(it)
                    mErrorMessage.postValue(it.message)
                })
        )
    }

    override fun onCleared() {
        if (!mDisposable.isDisposed)
            mDisposable.dispose()
        super.onCleared()
    }


}