package com.example.luther.letchat.utils

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.design.button.MaterialButton
import android.support.v4.content.ContextCompat.startActivity
import android.widget.TextView
import com.example.luther.letchat.R
import com.example.luther.letchat.ui.login.LoginActivity

object ActionAlertDialogUtils {
    interface ActionResultListener{
        fun onResultYes()
    }
    fun performActionAlertDialog(context: Context, alertMessage: String,callback : ActionResultListener){
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.action_alert_dialog_layout)
        dialog.findViewById<TextView>(R.id.tvAlertMessage).text = alertMessage
        dialog.show()
        dialog.findViewById<MaterialButton>(R.id.btnNo).setOnClickListener{
            dialog.dismiss()
        }
        dialog.findViewById<MaterialButton>(R.id.btnYes).setOnClickListener{
            callback.onResultYes()
        }
    }
}