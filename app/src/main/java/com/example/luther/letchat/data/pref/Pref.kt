package com.example.luther.letchat.data.pref

import android.content.Context
import android.content.SharedPreferences
import com.example.luther.letchat.utils.Const


class Pref {
    companion object {
        private const val PREFERENCES_NAME = "MyPref"
        fun getEditor(context: Context): SharedPreferences.Editor {
            return context
                .getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
                .edit()
        }

        fun getSharedPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        }

        fun getAccessToken(context: Context): String {
            var token = getSharedPreferences(context).getString(Const.ACCESS_TOKEN, "")
            if(!token!!.isEmpty()) token = "Bearer $token"
            return token
        }
    }


}
