package com.example.luther.letchat.data.model.response

data class CountResponse(
    val count: Int
)