package com.example.luther.letchat.ui.search

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.widget.Toast
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.Friend
import com.example.luther.letchat.data.model.User
import com.example.luther.letchat.data.model.response.StatusResponse
import com.example.luther.letchat.data.repo.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SearchActivityViewModel(application: Application) : AndroidViewModel(application) {
    private val userRepository = UserRepository(LetChatApiService.create(application))
    private val compositeDisposable = CompositeDisposable()


    private val _userList = MutableLiveData<List<Friend>>()
    val users: LiveData<List<Friend>>
        get() = _userList

    fun searchUser(userID: String) {
        compositeDisposable.add(
            userRepository.searchUser(userID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d(it.toString())
                    _userList.postValue(it.data)
                }, { error ->
                    Toast.makeText(getApplication(), error.message, Toast.LENGTH_SHORT).show()
                })
        )
    }

    fun sendFriendRequest(uid: String) {
        compositeDisposable.add(
            userRepository.addFriend(uid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    if (response.message.toLowerCase() == "ok") {
                        _userList.value?.let { users ->
                            val newList = users.filter {
                                it.uid != uid
                            }
                            _userList.postValue(newList)
                        }
                    }
                }, {
                    Timber.e(it)
                })
        )
    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed)
            compositeDisposable.dispose()
        super.onCleared()
    }
}
