package com.example.luther.letchat.utils

import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import com.bumptech.glide.Glide
import com.example.luther.letchat.R
import de.hdodenhof.circleimageview.CircleImageView

object ImageUtils {
    fun loadProfile(context: Context, profileImageView: CircleImageView, isOnline: Int) {
        if (isOnline == 1) profileImageView.borderColor = context.resources.getColor(R.color.colorOnlineBorder)
        Glide.with(context)
            .load(R.drawable.profile)
            .into(profileImageView)
    }
}

