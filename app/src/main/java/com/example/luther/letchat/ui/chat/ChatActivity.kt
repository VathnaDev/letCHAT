package com.example.luther.letchat.ui.chat

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.RemoteInput
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.custom.DialogMenu
import com.example.luther.letchat.custom.OnDialogItemClickListener
import com.example.luther.letchat.data.model.Message
import com.example.luther.letchat.ui.chat.adapter.ChatListAdapter
import com.example.luther.letchat.service.AppNotification
import com.example.luther.letchat.service.AppNotification.KEY_NOTIFICATION_ID
import com.example.luther.letchat.service.AppNotification.KEY_TEXT_REPLY
import com.example.luther.letchat.utils.Const
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions
import kotlinx.android.synthetic.main.activity_chat.*
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.util.*


class ChatActivity : AppCompatActivity() {
    private val mMessages = arrayListOf<Message>()
    private val mChatListAdapter = ChatListAdapter(mMessages)

    private lateinit var mViewModel: ChatActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val id = intent.getIntExtra(RECEIVER_ID, 0)

        mViewModel = ViewModelProviders.of(this)
            .get(ChatActivityViewModel::class.java)
        mViewModel.fetchReceiver(id)
        mViewModel.fetchMessages(id)

        rvMessageList.adapter = mChatListAdapter
        initListeners()
        observeData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.chat_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    private fun initListeners() {
        val emoji = EmojIconActions(
            this,
            window.decorView,
            etMessage,
            btnEmoji
        )
        emoji.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.ic_emoji)
        emoji.ShowEmojIcon()

        btnPickPhoto.setOnClickListener {
            DialogMenu(this, callback = object : OnDialogItemClickListener {
                override fun onItemClicked(result: Int, view: View) {
                    if (result == 1)
                        openCamera()
                    else if (result == 2)
                        openGallery()

                }
            })
        }
        btnSend.setOnClickListener {
            val message = etMessage.text.toString()
            if (message.isBlank() || message.isEmpty()) return@setOnClickListener
            mViewModel.sendMessage(message)
            etMessage.setText("")
        }

        rvMessageList.addOnLayoutChangeListener { _, _, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (mMessages.size > 0)
                rvMessageList.smoothScrollToPosition(
                    mMessages.size - 1
                )
        }

        etMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (count > 0)
                    mViewModel.emitTypingEvent()
                else
                    mViewModel.emitStopTypingEvent()
            }

        })

        etMessage.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus)
                mViewModel.emitStopTypingEvent()
        }


    }

    private fun observeData() {
        mViewModel.messages.observe(this, Observer { messages ->
            messages?.let {
                this.mMessages.clear()
                this.mMessages.addAll(it)
                mChatListAdapter.notifyItemInserted(mMessages.size - 1)
                if (it.size > 10)
                    rvMessageList.scrollToPosition(mMessages.size - 1)
                else if (it.isNotEmpty())
                    rvMessageList.smoothScrollToPosition(mMessages.size - 1)
            }
        })

        mViewModel.receiver.observe(this, Observer {
            tvReceiverName.text = it?.name
            tvStatus.text =
                    if (it?.isOnline == 1) "Online" else "last seen ${DateUtils.getRelativeTimeSpanString(it?.lastOnlineAt!!.time)}"

        })

        mViewModel.status.observe(this, Observer {
            tvStatus.text = it
        })

        mViewModel.message.observe(this, Observer {
            Toast.makeText(this@ChatActivity, it!!, Toast.LENGTH_SHORT).show()
        })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        else if (item?.itemId == R.id.chat_option_menu) {
            DialogMenu(this, object : OnDialogItemClickListener {
                override fun onItemClicked(result: Int, view: View) {
                    if (result == 1)
                        mViewModel.muteChat()
                    else
                        mViewModel.blockUser()
                }
            }, DialogMenu.BLOCK)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            OPEN_CAMERA_REQUEST_CODE -> if (resultCode == Activity.RESULT_OK) {
                data?.let {
                    val bitmap = it.extras?.get("data") as Bitmap
                    val photo = saveBitmapToFile(bitmap, UUID.randomUUID().toString())
                    mViewModel.sendPhoto(photo)
                }
            }
            OPEN_GALLERY_REQUEST_CODE -> if (resultCode == Activity.RESULT_OK) {
                data?.data?.let { uri ->
                    Timber.d("MediaType: ${contentResolver.getType(uri)}")
                    val file = File(getRealPathFromURI(uri))
                    mViewModel.sendPhoto(file)
                }
            }
        }
    }

    private fun getRealPathFromURI(contentUri: Uri): String {
        var cursor: Cursor? = null
        return try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = application.contentResolver.query(contentUri, proj, null, null, null)
            val columnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(columnIndex)
        } catch (e: Exception) {
            Timber.e("getRealPathFromURI Exception : $e")
            ""
        } finally {
            cursor?.close()
        }
    }

    private fun saveBitmapToFile(bitmap: Bitmap, name: String): File {
        val fileDir = application.filesDir
        val imageFile = File(fileDir, "$name.jpg")

        try {
            val os = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
            os.flush()
            os.close()
        } catch (ex: Exception) {
            Timber.e(ex)
        }
        return imageFile
    }


    fun openCamera() {
        if (checkCameraPermission()) {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(packageManager)?.also {
                    startActivityForResult(takePictureIntent, OPEN_CAMERA_REQUEST_CODE)
                }
            }
        }
    }

    fun openGallery() {
        if (checkStoragePermission()) {
            val mimeTypes = arrayOf("image/jpeg", "image/png", "image/jpg")
            val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            pickPhoto.type = "image/*"
            pickPhoto.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            startActivityForResult(Intent.createChooser(pickPhoto, "Select Picture"), OPEN_GALLERY_REQUEST_CODE)
        }
    }

    private fun checkCameraPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                Const.PERMISSION_CAMERA_REQUEST_CODE
            )
            return false
        }
        return true
    }

    private fun checkStoragePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                Const.PERMISSION_READ_STORAGE_REQUEST_CODE

            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Const.PERMISSION_CAMERA_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    openCamera()
                } else {
                    Toast.makeText(this, "We need your permission to access camera", Toast.LENGTH_SHORT).show()
                }
                return
            }

            Const.PERMISSION_READ_STORAGE_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    openGallery()
                } else {
                    Toast.makeText(this, "We need your permission to access gallery", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }

    override fun onDestroy() {
        LetChatApp.receiverId = 0
        super.onDestroy()
    }

    companion object {
        const val RECEIVER_ID: String = "receiver_id"
        const val OPEN_CAMERA_REQUEST_CODE = 1
        const val OPEN_GALLERY_REQUEST_CODE = 2
    }
}
