package com.example.luther.letchat.utils

import com.bumptech.glide.Glide
import com.example.luther.letchat.R
import de.hdodenhof.circleimageview.CircleImageView
import com.github.marlonlom.utilities.timeago.TimeAgo
import com.github.marlonlom.utilities.timeago.TimeAgoMessages
import java.util.*


fun CircleImageView.loadProfile(url: String, isOnline: Int) {
    if (isOnline == 1)
        this.borderColor = context.getColor(R.color.colorOnlineBorder)
    Glide.with(context)
        .load("https://s3-alpha-sig.figma.com/img/c8c8/d800/47be7fd70e3cab1622f2beda8efa377e?Expires=1549238400&Signature=RkPnPh8ZgcPTyxY0B19sKF-W1xCU0rhPvXCGdKdSnicnSsxYclibW9lvjvv7rm-NkFuxbj7ndmeDRsLAZtqGMb5aWMCei4s5uFvRUXYMn6F2hZDT00PcvfLdU17mAVij7q8aOhNyFUsW1b~GzqOGbJ4tUyUP597m-JMw1FQyDf5nZIC8jvVLuoNDdMsXpdW1P3BQ1lAuNJya0yh3j0O3Vm7p2jW7dziaXaiDXosQNp8o~VLy0fkNg~4U4ZzA0LENG1vTpbvUfBWtInvHufstgsA6J6neNt5iYN-AmJ9zWLZ2HVtM5QbtBXTxxj3rizKqsf15PEPQAzqO4dR5GfX-TA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA")
        .into(this)
}

var LocaleBylanguageTag = Locale.forLanguageTag("en")
var messages = TimeAgoMessages.Builder().withLocale(LocaleBylanguageTag).build()


fun Date.toTimeAgo(): String {
    return TimeAgo.using(this.time, messages)
}