package com.example.luther.letchat.ui.main.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.luther.letchat.ui.friend.FriendFragment
import com.example.luther.letchat.ui.inbox.InboxFragment
import com.example.luther.letchat.ui.setting.SettingFragment

class MainTabPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(index: Int): Fragment {
        return when (index) {
            0 -> return InboxFragment()
            1 -> return FriendFragment()
            2 -> return SettingFragment()
            else -> Fragment()
        }
    }
    override fun getCount(): Int {
        return 3
    }

}