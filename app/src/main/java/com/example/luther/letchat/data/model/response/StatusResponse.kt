package com.example.luther.letchat.data.model.response

data class StatusResponse(
    val message: String,
    val statusCode: Int?
)