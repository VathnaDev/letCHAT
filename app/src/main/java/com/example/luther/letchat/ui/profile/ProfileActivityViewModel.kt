package com.example.luther.letchat.ui.profile

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.response.StatusResponse
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.utils.Const
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class ProfileActivityViewModel(private val app: Application) : AndroidViewModel(app) {
    private val mDisposable = CompositeDisposable()
    private val apiService = LetChatApiService.create(app)


    fun deleteFcmToken() : Observable<StatusResponse>{
        val token = Pref.getSharedPreferences(app)
            .getString(Const.FIREBASE_TOKEN,"")
        token?.let {
            return apiService.deleteToken(it)
        }
    }
}