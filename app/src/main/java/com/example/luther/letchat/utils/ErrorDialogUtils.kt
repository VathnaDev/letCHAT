package com.example.luther.letchat.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.TextView
import com.example.luther.letchat.R

object ErrorDialogUtils {
    fun performErrorMessageDialog(context: Context, errorMessage: String){
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.error_dialog_layout)
        dialog.findViewById<TextView>(R.id.tvErrorMessage).text = errorMessage
        dialog.show()
    }
}