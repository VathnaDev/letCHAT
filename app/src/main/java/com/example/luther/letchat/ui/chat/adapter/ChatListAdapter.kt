package com.example.luther.letchat.ui.chat.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.data.model.Message
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import java.util.*
import android.widget.TextView
import com.example.luther.letchat.R
import java.text.SimpleDateFormat


class ChatListAdapter(private val messages: List<Message>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val otherDayFormat = SimpleDateFormat("EEE, h:mm a", Locale("en"))
    private val todayFormat = SimpleDateFormat("h:mm a", Locale("en"))
    private val df = SimpleDateFormat("yyyyMMdd", Locale("en"))

    private val currentUserId: Int = LetChatApp.user.id
    private lateinit var context: Context


    override fun getItemViewType(position: Int): Int {
        val message = messages[position]
        val senderId = message.senderId

        return if (senderId == currentUserId)
            VIEW_TYPE_MESSAGE_SENT
        else
            VIEW_TYPE_MESSAGE_RECEIVED
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        context = viewGroup.context

        return if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(context).inflate(R.layout.message_list_send_item, viewGroup, false)
            SentMessageViewHolder(view)
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.message_list_receive_item, viewGroup, false)
            ReceivedMessageViewHolder(view)
        }
    }

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message = messages[position]

        var previousTimeStamp = 0L
        if (position > 0) {
            val msg = messages[position - 1]
            previousTimeStamp = msg.sentAt.time
        }
        when (holder.itemViewType) {
            VIEW_TYPE_MESSAGE_SENT -> {
                val viewHolder = (holder as SentMessageViewHolder)
                viewHolder.bind(message)
                setTimeTextVisibility(
                    message.sentAt.time,
                    previousTimeStamp,
                    viewHolder.itemView.findViewById(R.id.tvMessageDate)
                )
            }
            VIEW_TYPE_MESSAGE_RECEIVED -> {
                val viewHolder = (holder as ReceivedMessageViewHolder)
                viewHolder.bind(message)
                setTimeTextVisibility(
                    message.sentAt.time,
                    previousTimeStamp,
                    viewHolder.itemView.findViewById(R.id.tvMessageDate)
                )
            }
        }
    }

    private fun setTimeTextVisibility(ts1: Long, ts2: Long, timeText: TextView) {
        if (ts2 == 0L) {
            timeText.visibility = View.VISIBLE
            timeText.text = getDateFormat(Date(ts1)).format(Date(ts1))
        } else {
            val sameDate = (df.format(Date(ts1)) == df.format(Date(ts2)))
            if (sameDate) {
                timeText.visibility = View.GONE
                timeText.text = ""
            } else {
                timeText.visibility = View.VISIBLE
                timeText.text = getDateFormat(Date(ts1)).format(Date(ts1))
            }

        }
    }

    private fun getDateFormat(date: Date): SimpleDateFormat {
        return if (df.format(Date()) == df.format(date))
            todayFormat
        else otherDayFormat
    }

    companion object {
        private const val VIEW_TYPE_MESSAGE_SENT = 1
        private const val VIEW_TYPE_MESSAGE_RECEIVED = 2
    }

}


