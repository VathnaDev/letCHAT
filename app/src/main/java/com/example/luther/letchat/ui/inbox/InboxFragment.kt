package com.example.luther.letchat.ui.inbox

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.Message
import com.example.luther.letchat.service.MessageEvent
import com.example.luther.letchat.ui.inbox.adapter.MessageInboxAdapter
import kotlinx.android.synthetic.main.fragment_inbox.*
import org.greenrobot.eventbus.EventBus


class InboxFragment : Fragment() {
    private val mMessagesList = arrayListOf<Message>()
    private val mInboxAdapter = MessageInboxAdapter(mMessagesList)

    private lateinit var mViewModel: InboxFragmentViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_inbox, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel = ViewModelProviders.of(activity!!)
            .get(InboxFragmentViewModel::class.java)

        rvMessageList.adapter = mInboxAdapter
        initListeners()
        observeData()
    }

    override fun onStart() {
        super.onStart()
        mViewModel.fetchMessages()
    }



    private fun initListeners() {
        rvMessageList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (layoutManager.findFirstCompletelyVisibleItemPosition() == 1)
                    mViewModel.loadMore()
            }
        })
    }

    private fun observeData() {
        mViewModel.messages.observe(this, Observer { messages ->
            mMessagesList.clear()
            mMessagesList.addAll(messages ?: arrayListOf())
            mInboxAdapter.notifyDataSetChanged()

            var counter = 0
            mMessagesList.forEach {
                if (it.seen.toLowerCase() == "unseen" && it.senderId != LetChatApp.user.id)
                    counter++
            }
            LetChatApp.unSeenMessageCount.value = counter
        })
    }

}
