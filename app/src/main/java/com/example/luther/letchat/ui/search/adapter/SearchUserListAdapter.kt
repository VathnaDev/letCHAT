package com.example.luther.letchat.ui.search.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.luther.letchat.R
import com.example.luther.letchat.data.model.Friend
import kotlinx.android.synthetic.main.search_by_id_item.view.*

class SearchUserListAdapter(
    private val userList: ArrayList<Friend>,
    private val eventCallback: EventCallback
) : RecyclerView.Adapter<SearchUserListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SearchUserListAdapter.ViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(p0.context)
        val view: View = inflater.inflate(R.layout.search_by_id_item, p0, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: SearchUserListAdapter.ViewHolder, position: Int) {
        val user: Friend = userList[position]
        viewHolder.bind(user)
    }

    override fun getItemCount(): Int = userList.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: Friend) {
            itemView.txtUserName.text = user.name
            itemView.btnAdd.setOnClickListener {
                eventCallback.onAddFriendClickListener(user)
            }
        }
    }

    interface EventCallback {
        fun onAddFriendClickListener(user: Friend)
    }
}