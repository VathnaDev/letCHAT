package com.example.luther.letchat.ui.inbox

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.Message
import com.example.luther.letchat.data.model.response.Metadata
import com.example.luther.letchat.data.repo.UserRepository
import com.github.nkzawa.emitter.Emitter
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class InboxFragmentViewModel(app: Application) : AndroidViewModel(app) {
    private var mMetadata: Metadata = Metadata()
    private val mDisposable = CompositeDisposable()
    private val mUserRepo = UserRepository(LetChatApiService.create(app))

    private val _inboxMessages = MutableLiveData<List<Message>>()
    val messages: LiveData<List<Message>>
        get() = _inboxMessages

    private val onErrorListener = Emitter.Listener { errors ->
        errors.forEach {
            Timber.d(it.toString())
        }
    }

    init {
        fetchMessages()
        LetChatApp.mSocket.on("chat") { messages ->
            fetchMessages()
        }.on("exception", onErrorListener)
    }

    fun fetchMessages() {
        mDisposable.add(
            mUserRepo.getRecentChatInbox()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _inboxMessages.postValue(it.data)
                    mMetadata = it.metadata
                }, {
                    Timber.d(it)
                })
        )
    }

    fun loadMore() {
    }

    override fun onCleared() {
        if (!mDisposable.isDisposed)
            mDisposable.dispose()
        super.onCleared()
    }


}