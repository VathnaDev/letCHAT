package com.example.luther.letchat.ui.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Patterns
import android.view.View
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.ui.main.MainActivity
import com.example.luther.letchat.ui.signup.SignUpActivity
import com.example.luther.letchat.ui.signup.SignUpWithFacebook.SignUpWithFacebookActivity
import com.example.luther.letchat.utils.ErrorDialogUtils
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import kotlinx.android.synthetic.main.activity_login.*
import timber.log.Timber
import java.security.MessageDigest

class LoginActivity : AppCompatActivity() {

    private lateinit var mFacebookLoginButton: LoginButton
    private lateinit var mViewModel: LoginActivityViewModel

    private lateinit var mCallbackManager: CallbackManager
    private lateinit var accessToken: AccessToken

    private var mFbName = ""
    private var mFbImageUrl = ""
    private var mFbTokenKey = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(this)
        setContentView(R.layout.activity_login)

        mViewModel = ViewModelProviders.of(this)
                .get(LoginActivityViewModel::class.java)

        mFacebookLoginButton = LoginButton(this)
        mFacebookLoginButton.setReadPermissions("public_profile")
        mFacebookLoginButton.setReadPermissions("email")
        mCallbackManager = CallbackManager.Factory.create()

        loginProgressBar?.visibility = View.INVISIBLE

        setupListener()
        generateFacebookKeyHash()
        checkFacebookLoginState()
        observeData()
    }

    private fun setupListener() {
        btnLogin?.setOnClickListener {
            val email = etEmail?.text.toString()
            val password = etPassword?.text.toString()
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches() || password.length < 6) {
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    etEmail?.error = "Invalid Email Address"
                }
                if (password.length < 6) {
                    etPassword?.error = "Password cannot be blank or less than 6 characters"
                }
                return@setOnClickListener
            }
            performLogin(email, password)
        }
        btnFbLogin?.setOnClickListener {
            onLoginWithFacebook()
        }

        btnShowSignUp?.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }
    }

    private fun generateFacebookKeyHash() {
        try {
            val info: PackageInfo = packageManager.getPackageInfo("com.example.luther.letchat", PackageManager.GET_SIGNATURES)
            for (signature: Signature in info.signatures) {
                val messageDigest: MessageDigest = MessageDigest.getInstance("SHA")
                messageDigest.update(signature.toByteArray())
                Timber.d("Keyhash: ${Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT)}")
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.d("Key hash not found...")
        }
    }

    private fun onLoginWithFacebook() {
        mFacebookLoginButton.performClick()
        mFacebookLoginButton.registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                /*****/
                accessToken = AccessToken.getCurrentAccessToken()
                val request: GraphRequest = GraphRequest.newMeRequest(
                        result?.accessToken
                ) { `object`, response ->

                    val id: String = `object`.getString("id")
                    mFbName = `object`.getString("name")
                    mFbImageUrl = "http://graph.facebook.com/" + id + "/picture?type=large"
                    mFbTokenKey = accessToken.token.toString()
                    performFacebookLogin(mFbTokenKey)
                }
                val parameter = Bundle()
                parameter.putString("fields", "id, name, email")
                request.parameters = parameter
                request.executeAsync()
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {

            }

        })
    }

    private fun performLogin(email: String, password: String) {
        loginProgressBar?.visibility = View.VISIBLE
        mViewModel.loginUser(email, password)
    }

    private fun observeData() {
        mViewModel.currentUser.observe(this, Observer { user ->
            user?.let {
                loginProgressBar?.visibility = View.INVISIBLE
                LetChatApp.user = it
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        })

        mViewModel.errorMessage.observe(this, Observer {
            loginProgressBar?.visibility = View.INVISIBLE
            val errorResponse = when {
                it.toString().contains("400") -> "Invalid email or password!"
                it.toString().contains("502") -> "Weak connection!"
                else -> "Something went wrong. \nPlease try again later!"
            }
            ErrorDialogUtils.performErrorMessageDialog(this, errorResponse)
        })

        mViewModel.failLoginWithFacebook.observe(this, Observer {
            loginProgressBar?.visibility = View.INVISIBLE
            if (it.toString().contains("400")){
                startSignUpWithFacebookActivity()
            } else {
                val errorResponse = when {
                    it.toString().contains("502") -> "Weak connection!"
                    else -> "Something went wrong. \nPlease try again later!"
                }
                ErrorDialogUtils.performErrorMessageDialog(this, errorResponse)
            }
        })
    }

    private fun startSignUpWithFacebookActivity() {
        val intent = Intent(this, SignUpWithFacebookActivity::class.java)
        intent.putExtra("tokenKey", mFbTokenKey)
        intent.putExtra("name", mFbName)
        intent.putExtra("imageUrl", mFbImageUrl)
        startActivity(intent)
    }

    private fun performFacebookLogin(tokenKey: String) {
        loginProgressBar?.visibility = View.VISIBLE
        mViewModel.loginWithFacebook(tokenKey)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun checkFacebookLoginState() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return //already sign out
        }
        GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permission/", null, HttpMethod.DELETE, GraphRequest.Callback {
            LoginManager.getInstance().logOut()
        }).executeAsync()
    }
}
