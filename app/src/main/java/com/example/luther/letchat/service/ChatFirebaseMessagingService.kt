package com.example.luther.letchat.service

import android.content.Intent
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.data.api.LetChatApiService
import com.example.luther.letchat.data.model.NotificationRequest
import com.example.luther.letchat.ui.main.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class ChatFirebaseMessagingService : FirebaseMessagingService() {
    private val disposable = CompositeDisposable()
    private var gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Timber.d(remoteMessage.data.toString())

        val request = gson.fromJson<NotificationRequest>(
            gson.toJsonTree(remoteMessage.data, Map::class.java),
            NotificationRequest::class.java
        )

        when (request.screenType) {
            "requested" -> {
                AppNotification.pushFriendRequestNotification(
                    this,
                    request
                )
            }
            "confirmed" -> {
                AppNotification.pushAcceptFriendRequestNotification(
                    this,
                    request
                )
            }
            else
            -> {
                if (LetChatApp.receiverId != request.senderId) {
                    AppNotification.pushChatNotification(
                        this,
                        request
                    )
                }
            }
        }
    }

    override fun onNewToken(token: String?) {
        val apiService = LetChatApiService.create(applicationContext)

        disposable.add(
            apiService.createToken(token!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d(it.message)
                }, {
                    Timber.e(it)
                })
        )
    }

    override fun onDestroy() {
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
        super.onDestroy()
    }
}