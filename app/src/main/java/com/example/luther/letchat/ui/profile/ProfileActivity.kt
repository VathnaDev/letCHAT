package com.example.luther.letchat.ui.profile

import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.luther.letchat.R
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import kotlinx.android.synthetic.main.activity_profile.*
import java.lang.Exception
import android.widget.ImageView
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.ui.login.LoginActivity
import com.example.luther.letchat.ui.main.MainActivity
import com.example.luther.letchat.ui.main.MainActivityViewModel
import com.example.luther.letchat.utils.Const
import com.facebook.AccessToken
import com.facebook.FacebookSdk
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.facebook.login.LoginManager
import com.journeyapps.barcodescanner.BarcodeEncoder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class ProfileActivity : AppCompatActivity() {
    private val mDisposable = CompositeDisposable()
    private lateinit var mViewModel: ProfileActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(this)
        setContentView(R.layout.activity_profile)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mViewModel = ViewModelProviders.of(this)
            .get(ProfileActivityViewModel::class.java)

        val user = LetChatApp.user
        ivQR.setOnClickListener {
            val writer = MultiFormatWriter()
            try {

                val bitMatrix = writer.encode(user.uid, BarcodeFormat.QR_CODE, 250, 250)
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                val dialog = Dialog(this)
                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.setContentView(R.layout.dialog_qr_code)
                dialog.findViewById<ImageView>(R.id.ivQR).setImageBitmap(bitmap)
                dialog.show()
            } catch (e: Exception) {

            }

        }
    }

    override fun onStart() {
        super.onStart()
        val user = LetChatApp.user
        tvName.text = user.name
        tvUId.text = user.uid
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile_menu, menu)
        return true
    }

    /*Check facebook login state.
    * if user login with facebook, then sign out from facebook when clicking sign out button*/
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        else if (item?.itemId == R.id.logout_option_menu) {
            mDisposable.add(
                mViewModel.deleteFcmToken()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        Timber.d(it.toString())
                        Pref.getEditor(this)
                            .remove(Const.ACCESS_TOKEN)
                            .remove(Const.FIREBASE_TOKEN)
                            .commit()
                        checkFacebookLoginState()
                        startActivity(Intent(this, LoginActivity::class.java))
                        finishAffinity()
                    }, {
                        Timber.e(it)
                    })
            )
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkFacebookLoginState() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return //already sign out
        }
        GraphRequest(
            AccessToken.getCurrentAccessToken(),
            "/me/permission/",
            null,
            HttpMethod.DELETE,
            GraphRequest.Callback {
                LoginManager.getInstance().logOut()
            }).executeAsync()
    }
}
