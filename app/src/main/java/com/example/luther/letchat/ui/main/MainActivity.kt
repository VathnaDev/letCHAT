package com.example.luther.letchat.ui.main

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.luther.letchat.LetChatApp
import com.example.luther.letchat.R
import com.example.luther.letchat.custom.DialogMenu
import com.example.luther.letchat.custom.OnDialogItemClickListener
import com.example.luther.letchat.data.pref.Pref
import com.example.luther.letchat.ui.login.LoginActivity
import com.example.luther.letchat.ui.main.adapter.MainTabPagerAdapter
import com.example.luther.letchat.ui.profile.ProfileActivity
import com.example.luther.letchat.ui.scan.ScanQrCodeActivity
import com.example.luther.letchat.ui.search.SearchActivity
import com.example.luther.letchat.utils.Const
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber


class MainActivity : AppCompatActivity() {
    private lateinit var mViewModel: MainActivityViewModel
    private val mMainTabPagerAdapter: MainTabPagerAdapter = MainTabPagerAdapter(supportFragmentManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialize()
    }
    private fun initialize() {
        val token = Pref.getAccessToken(this)
        if (token.isEmpty()) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            LetChatApp.setupSocket(token)

            mViewModel = ViewModelProviders.of(this)
                .get(MainActivityViewModel::class.java)

            mViewModel.getCurrentUser()
            mainViewPager.adapter = mMainTabPagerAdapter
            mainViewPager.offscreenPageLimit = 3
            observeData()

            val fToken = Pref.getSharedPreferences(this)
                .getString(Const.FIREBASE_TOKEN, "")
            if (fToken.isNullOrEmpty()) {
                mViewModel.createFcmToken()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        isRunning = true
        initListeners()
    }

    private fun initListeners() {
        ivReceiverProfile.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
        }

        mainViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {
                changeTabs(p0)
            }

        })

        btnFindFriend.setOnClickListener {
            DialogMenu(this, object : OnDialogItemClickListener {
                override fun onItemClicked(result: Int, view: View) {
                    if (result == 1)
                        startScanner()
                    else
                        startActivity(Intent(this@MainActivity, SearchActivity::class.java))
                }

            }, DialogMenu.SEARCH)
        }

        btnChatTab.setOnClickListener { mainViewPager.currentItem = 0 }
        btnFriendTab.setOnClickListener { mainViewPager.currentItem = 1 }
        btnSettingTab.setOnClickListener { mainViewPager.currentItem = 2 }

    }

    private fun observeData() {
        LetChatApp.unSeenMessageCount.observe(this, Observer {
            if (it == 0)
                unSeenBadge?.visibility = View.GONE
            else {
                unSeenBadge?.visibility = View.VISIBLE
                tvUnSeenMessageCount?.text = it.toString()
            }
        })

        LetChatApp.friendRequestCount.observe(this, Observer {
            if (it == 0)
                requestCountBadge?.visibility = View.GONE
            else {
                requestCountBadge?.visibility = View.VISIBLE
                tvRequestCount?.text = it.toString()
            }
        })
    }


    private fun changeTabs(position: Int) {
        when (position) {
            0 -> {
                btnFindFriend.visibility = View.GONE
                tvTitle.text = "letCHAT"
                btnChatTab.setBackgroundResource(R.drawable.tab_active_background)
                btnFriendTab.setBackgroundResource(R.drawable.tab_in_active_background)
                btnSettingTab.setBackgroundResource(R.drawable.tab_in_active_background)
            }
            1 -> {
                btnFindFriend.visibility = View.VISIBLE
                tvTitle.text = "Friends"
                btnChatTab.setBackgroundResource(R.drawable.tab_in_active_background)
                btnFriendTab.setBackgroundResource(R.drawable.tab_active_background)
                btnSettingTab.setBackgroundResource(R.drawable.tab_in_active_background)
            }
            else -> {
                btnFindFriend.visibility = View.GONE
                tvTitle.text = "Setting"
                btnChatTab.setBackgroundResource(R.drawable.tab_in_active_background)
                btnFriendTab.setBackgroundResource(R.drawable.tab_in_active_background)
                btnSettingTab.setBackgroundResource(R.drawable.tab_active_background)
            }
        }
    }

    fun startScanner() {
        val hasPermission = checkPermission()
        if (hasPermission) {
            startActivityForResult(Intent(this, ScanQrCodeActivity::class.java), SCANNER_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_CAMERA -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startScanner()
                } else {
                    Toast.makeText(this, "We need your permission to access camera", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SCANNER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val result = data?.getStringExtra("result")
            if (!result.isNullOrEmpty()) {
                mViewModel.sendFriendRequest(result)
                    .observe(this, Observer {
                        Timber.d(it.toString())
                        if (it?.message?.toLowerCase() == "ok") {
                            Toast.makeText(this, "Friend request sent", Toast.LENGTH_SHORT).show()
                        }
                    })
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        isRunning = false
    }

    private fun checkPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                PERMISSIONS_REQUEST_CAMERA
            )
            return false
        }
        return true
    }

    companion object {
        const val PERMISSIONS_REQUEST_CAMERA: Int = 1
        const val SCANNER_REQUEST_CODE: Int = 2
        var isRunning = true
    }

}
