package com.example.luther.letchat.ui.login

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.example.luther.letchat.R

class LoginProgressDialog : DialogFragment(){
    private lateinit var mLoginProgressBar: ProgressBar
    private lateinit var mDialogLayout: LinearLayout
    private lateinit var tvLoadingText: TextView

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {

            mDialogLayout = LinearLayout(it)
            val layoutParam: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            mDialogLayout.gravity = Gravity.CENTER
            mDialogLayout.setBackgroundColor(Color.TRANSPARENT)
            mDialogLayout.orientation = LinearLayout.VERTICAL
            mDialogLayout.setBackgroundColor(Color.rgb(211, 211, 211))
            mDialogLayout.layoutParams = layoutParam

            mLoginProgressBar = ProgressBar(it)
            val pgLayoutParam = LinearLayout.LayoutParams(LinearLayout.LayoutParams(80,80))
            mLoginProgressBar.visibility = View.VISIBLE
            mLoginProgressBar.layoutParams = pgLayoutParam
            mDialogLayout.addView(mLoginProgressBar)

            tvLoadingText = TextView(it)
            val tvLayoutParam = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            tvLoadingText.text = "Loading..."
            tvLoadingText.layoutParams = tvLayoutParam
            mDialogLayout.addView(tvLoadingText)

            val builder = AlertDialog.Builder(it)
            builder.setView(mDialogLayout)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}