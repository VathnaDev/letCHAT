package com.example.luther.letchat.data.model

import java.util.*

data class Message(
    val id: Int = 0,
    val content: String,
    var gallery: String = "",
    val receiverId: Int,
    val name : String = "",
    val senderId: Int = 0,
    val sentAt: Date = Date(),
    val seenAt: Date = Date(),
    val seen: String = "",
    val uid: String = ""

)